paint = (msg, c) -> "#{c}#{msg}\x1b[0m"

pluralize = (word, count) ->
  if count > 1
    word + 's'
  else
    word

reportEvents = [
  'create', 'createError'
  'update', 'updateError'
  'unsatisfied', 'ignore'
  'unlink', 'unlinkError'
]

reportPathes = [
  'create', 'createError'
  'update', 'updateError'
  'unsatisfied', 'ignore'
  'unlink', 'unlinkError'
]

reportMessages =
  create: 'Created: '
  update: 'Updated: '
  unlink: 'Unlinked: '
  untouched: 'Untouched: '
  createError: 'Create failed: '
  updateError: 'Update failed: '
  unlinkError: 'Unlink failed: '
  unsatisfied: 'Unsatisfied: '
  ignore: 'Ignored: '
  absent: 'Requires one of: '

colors =
  white: "\x1b[97m"
  green: "\x1b[32m"
  yellow: "\x1b[33m"
  cyan: "\x1b[36m"
  orange: "\x1b[35m"
  blue: "\x1b[34m"
  red: "\x1b[91m"

colors.fromTo = colors.yellow
colors.path = colors.white
colors.ruleName = colors.yellow
colors.handlerName = colors.white
colors.handlersPipe = colors.orange
colors.resolversPipe = colors.yellow

colors.create = colors.green
colors.update = colors.yellow
colors.ignore = colors.cyan
colors.unlink = colors.orange
colors.untouched = colors.blue
colors.absent = colors.red
colors.unsatisfied = colors.red
colors.createError = colors.red
colors.updateError = colors.red
colors.unlinkError = colors.red

module.exports = class Report
  rules: []
  result: []

  add: (status, resolver, error) ->
    for r in @rules
      if resolver.rule.isPrototypeOf r
        target = r
        break
    if target
      rule = target
    else
      @rules.push rule = Object.create resolver.rule
      rule.handlers = []

    target = null
    for h in rule.handlers
      if resolver.handler.isPrototypeOf h
        target = h
        break
    if target
      handler = target
    else
      rule.handlers.push handler = Object.create resolver.handler
      handler.resolvers = []

    handler.resolvers.push resolver = Object.create resolver
    resolver.status = status
    resolver.error = error
    return

  print: ->
    for rule in @rules
      @printRule rule
    process.stdout.write @result.join ''
    return

  printRule: (rule) ->
    @result.push paint '╔═ ', colors.handlersPipe
    @result.push paint(rule.name, colors.ruleName) + ' ' if rule.name
    dests = (dest for key, dest of rule.to)
    @result.push paint(rule.from.join(','), colors.path),
      paint(' => ', colors.fromTo),
      paint(dests.join(', '), colors.path),
      '\n'
    last = rule.handlers.length - 1
    for handler, i in rule.handlers
      @printHandler handler, i is last
    return

  printHandler: (handler, lastHandler) ->
    sign = if lastHandler then '╚═ ' else '╠═ '
    @result.push paint(sign, colors.handlersPipe)
    @result.push @getHandlerInfo(handler), '\n'
    
    if lastHandler
      prefix = '   '
    else
      prefix = paint '║  ', colors.handlersPipe
    last = handler.resolvers.length - 1
    for resolver, i in handler.resolvers
      @printResolver resolver, i is last, prefix
    return

  getHandlerInfo: (handler) ->
    {map} = handler
    if map
      pipes = @getPipesInfo map
      chains = @getChainsInfo map
    s = paint "#{handler.name}", colors.handlerName
    if pipes
      s += pipes
    if pipes or chains
      s = "(#{s})"
    if chains
      s += chains
    s

  getPipesInfo: (map) ->
    pipes = ''
    for key, value of map
      if value.pipes
        for pipe in value.pipes
          pipes += ' | '
          pipes += key + ': ' unless key is 'all'
          pipes += @getHandlerInfo pipe.handler
    pipes

  getChainsInfo: (map) ->
    chains = ''
    for key, value of map
      if value.chains
        for chain in value.chains
          chains += ' -> '
          chains += key + ': ' unless key is 'all'
          chains += @getHandlerInfo chain.handler
    chains

  printResolver: (resolver, lastResolver, prefix) ->
    {status} = resolver

    @result.push(
      prefix
      if lastResolver then '└' else '├'
      "─ "
      colors.resolversPipe
    )

    @result.push paint(reportMessages[status], colors[status]),
      paint(resolver.entry.rel(resolver.rule.root), colors.path)

    if resolver.error
      @result.push "\n    " + resolver.error + resolver.error.stack

    if status is 'create' or status is 'update' or status is 'unlink'
      for key, dest of resolver.destinations.map
        @result.push paint(' -> ', colors.resolversPipe),
          paint dest.entry.rel(resolver.rule.root), colors.path
    
    @result.push '\n'
