Entry = require '../entry'
watch = require './watch'
process = require './process'
{sep} = Entry
{push} = Array.prototype

{success: defaultSuccess, error: defaultError} = require './default_messages'

module.exports = class Resolver extends require 'events'
  constructor: (@entry, @rule, @handler) ->
    super()
    @kickassets = @rule.kickassets
    @watching = 0

  Object.assign @prototype, watch

  setDestinations: (@pattern, @suffix) ->
    @destinations = new @kickassets.Destinations @kickassets, @, @pattern, @suffix
    return

  getRelativePath: ->
    unless @relativePath
      @setRelativePath()
    @relativePath

  setRelativePath: ->
    @relativePath = @entry.path.slice @rule.offsets[@pattern]
    @relativePath = @relativePath.slice 0, -@suffix.length if @suffix
    return

  processNewer: (reporter) ->
    existPromise = @destinations.exist()
    satisfiedPromise = @getDependencies().areSatisfied()
    [exist, satisfied] = await Promise.all [existPromise, satisfiedPromise]
    unless satisfied
      reporter.add 'unsatisfied', @
      return
    
    if not exist
      event = 'create'
    else
      mtime = await @getMtime()
      if mtime > await @destinations.getMtime()
        event = 'update'
      else
        reporter.add 'untouched', @
        return

    try
      await @process promises = []
      await Promise.all promises
    catch err
      return reporter.add "#{event}Error", @, err
    reporter.add event, @
    return

  getDependencies: ->
    unless @dependencies
      @setDependencies()
    @dependencies

  setDependencies: ->
    @dependencies = new @kickassets.Dependencies @kickassets, @
    return

  getMtime: ->
    return @mtime if @mtime?
    return false unless @dependencies.areSatisfied()

    times = @dependencies.getMtimes()
    times.push @entry.getMtime()
    @mtime = Math.max.apply Math, await Promise.all times

  read: ->
    unless @content?
      @setContent await @entry.read()
    @content

  setContent: (@content) ->

  getResolver: ->
    @

  process: (promises) ->
    await process @, promises
    return

  write: (results) ->
    if @destinations
      new Promise (resolve, reject) =>
        @destinations.write(results)
          .then =>
            @success()
            resolve()
          .catch (err) =>
            @error err
            reject()
    else
      @success()

  unlink = (destination) ->
    destination.unlink().then (entry) =>
      @emit 'unlink'
      console.log "remove #{entry.path}"

  unlinkDeep: (promises, arr, resolver, destination) ->
    for item in arr
      item.resolver = resolver
      item.setDest destination
      promises.push item.unlinkDest()

  unlinkDest: ->
    promises = []

    if @destinations
      for key, destination of @destinations.map
        promises.push unlink.call @, destination
      for key, value of @handler.map
        if value.pipes
          for key, destination of @destinations.map
            unlinkDeep promises, value.pipes, @getResolver(), destination
        if value.chains
          for key, destination of @destinations.map
            unlinkDeep promises, value.chains, @getResolver(), destination

    await Promise.all promises
    @emit 'unlink'

  getResults: ->
    unless @results?
      await @setResults()
    @results

  setResults: ->
    @results = await @getPipedResults()
    return

  getPipedResults: (type) ->
    result = await @handler.config.make @
    if typeof result isnt 'object' or Buffer.isBuffer result
      results = {}
      results[type or 'default'] = result
    else
      results = result
    await @kickassets.Pipe.process @, results
    results

  success: (destination) ->
    if @handler.config.success
      @handler.config.success @, destination
    else
      defaultSuccess @, destination

  error: (err) ->
    if @handler.config.error
      @handler.config.error @, err
    else
      defaultError @, err
