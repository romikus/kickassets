module.exports = class Pipe
  constructor: (@handler) ->

  @extendHandler: (handler, args) ->
    h = handler.extend()
    for arg in args
      if arg.constructor.name is 'Handler'
        ((h.map.all or= {}).pipes or= []).push new Pipe arg
      else
        for i, pipes of arg
          map = (h.map[i] or= {})
          pipes = [pipes] unless Array.isArray pipes
          for pipe in pipes
            (map.pipes or= []).push new Pipe pipe
    h

  @process: (resolver, results) ->
    processed = await @processResults resolver, results
    @replaceResults results, processed
    return

  @processResults: (resolver, results) ->
    map = resolver.handler.map
    Promise.all(
      for type, result of results
        @processAllAndType resolver, map, type, result
    )

  @replaceResults: (results, processed) ->
    i = 0
    for type of results
      results[type] = processed[i++]
    return

  @processAllAndType: (resolver, map, type, result) ->
    result = await @processAll resolver, map.all, type, result
    result = await @processType resolver, map[type], type, result
    result

  @processAll: (resolver, all, type, result) ->
    if all and all.pipes
      result = await @processResult resolver, all.pipes, type, result
    result

  @processType: (resolver, hash, type, result) ->
    if hash and hash.pipes
      result = await @processResult resolver, hash.pipes, type, result
    result

  @processResult: (resolver, pipes, type, result) ->
    for pipe in pipes
      pipe.content = result
      pipe.kickassets = resolver.kickassets
      results = await pipe.kickassets.Resolver::getPipedResults.call pipe, type
      result = results[type]
      result = results.default unless result?
    result

  read: ->
    @content
