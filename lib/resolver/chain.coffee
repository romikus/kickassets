process = require './process'

module.exports = class Chain
  constructor: (handler) ->
    @handler = handler
    @handler.setDestinations()

  @extendHandler: (handler, args) ->
    h = handler.extend()
    for arg in args
      if arg.constructor.name is 'Handler'
        ((h.map.all or= {}).chains or= []).push new Chain arg
      else
        for i, chains of arg
          map = (h.map[i] or= {})
          chains = [chains] unless Array.isArray chains
          for chain in chains
            (map.chains or= []).push new Chain chain
    h

  @process: (resolver, results, promises) ->
    unless resolver.destinations
      return

    map = resolver.handler.map
    for type, result of results
      if map[type] and map[type].chains
        for chain in map[type].chains
          chain.process type, result, resolver, promises
    return

  process: (@type, @content, @resolver, promises) ->
    @kickassets = @resolver.kickassets
    await process @, promises
    return

  read: ->
    @content

  write: (results) ->
    result = results[@type]
    entry = await @getEntry()
    if result?
      await entry.write result
    else
      await entry.unlink()
    return

  getEntry: ->
    handlerDestination = @getDestination()
    resolverDestination = @getResolverDestination()
    filename = await resolverDestination.getFilename()
    filename += handlerDestination.suffix
    resolverDestination.dir.new filename

  getDestination: ->
    @handler.destinations[@type] or @handler.destinations.default

  getResolverDestination: ->
    @resolver.destinations.map[@type] or @resolver.destinations.map.default

  getResults: ->
    @kickassets.Resolver::getPipedResults.call @, @type
