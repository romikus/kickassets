module.exports = extend = (a, b) ->
  out = {}

  if typeof a is 'object'
    Object.assign out, a

  for key, value of b
    if typeof value is 'object'
      out[key] = extend out[key], value
    else
      out[key] = value

  out