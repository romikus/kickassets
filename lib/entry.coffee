{
  stat, watch, readdir, readFile, writeFile, unlink, exists, mkdir
  createReadStream, createWriteStream
} = require 'fs'

sep = require('path').sep
{defineProperty, keys} = Object
require './helpers'

map = {}
root = process.cwd()

EventEmitter = require 'events'
EventEmitter.defaultMaxListeners = 100

{
  on: addListener
  once: addOnceListener
  removeListener: removeListener
} = EventEmitter.prototype

mkdirPool = {}
mkdirRecursive = (path) -> new Promise (resolve, reject) ->
  exists path, (exists) ->
    return resolve() if exists
    cb = (err) ->
      return resolve() if not err or err is 'EEXIST'
      return reject err if err.code isnt 'ENOENT'
      try
        await mkdirRecursive path.slice 0, path.lastIndexOf sep
      catch err
        return reject err
      mkdir path, resolve
    if mkdirPool[path]
      mkdirPool[path].push cb
    else
      mkdirPool[path] = arr = [cb]
      mkdir path, (err) ->
        delete mkdirPool[path]
        cb err for cb in arr

class Watcher extends EventEmitter
  constructor: (@path, @dir) ->
    super()
    @target = Entry.new @path
  
  watchRecursive = (existing, deeper, add, remove) ->
    if deeper # not found
      await deeper.once 'add', onadd = =>
        deeper.removeListener 'remove', onremove
        if deeper is @target
          existing = @target
          deeper = null
        else
          [existing, deeper] = await deeper.getfurtherExisting @target, @dir
        watchRecursive.call @, existing, deeper, 'add', 'remove'
      await deeper.once 'remove', onremove = =>
        deeper.removeListener 'add', onadd
        [existing, deeper] = await deeper.getClosestExisting @dir
        watchRecursive.call @, existing, deeper, 'add', 'remove'
      @close = ->
        deeper.removeListener 'add', onadd
        deeper.removeListener 'remove', onremove
      @emit remove, @target
    else # found
      await @target.once 'remove', onremove = =>
        [existing, deeper] = await @target.getClosestExisting @dir
        watchRecursive.call @, existing, deeper, 'add', 'remove'
      @close = =>
        @target.removeListener 'remove', onremove
      @emit add, existing

  close: -> @closed = true
  
  start: ->
    @watch = @listenerCount('add') isnt 0 or @listenerCount('remove') isnt 0
    [existing, deeper] = await @target.getClosestExisting @dir
    return if @closed
    if @watch
      watchRecursive.call @, existing, deeper, 'found', 'not found'
    else if deeper
      @emit 'not found'
    else
      @emit 'found', existing
    
    if deeper then false else existing

module.exports = class Entry extends EventEmitter
  constructor: (path, @filename) ->
    super()
    map[@path = path] = @
    @changing = false

  @new: (path, name) ->
    map[path] or new Entry path, name or path.slice 1 + path.lastIndexOf sep

  @get: (path, name) ->
    entry = @new(path, name)
    if await entry.exist()
      entry

  @sep: sep
  
  @mkdirRecursive: mkdirRecursive
  
  @watch: (path, dir) -> new Watcher path, dir

  @copy: (from, to) -> new Promise (resolve, reject) ->
    try
      await mkdirRecursive to.slice 0, to.lastIndexOf sep
    catch err
      return reject err
    createReadStream(from)
      .pipe(createWriteStream(to).on 'close', resolve)
      .on 'error', reject

  @rel: (path, r) ->
    path.slice (r or root).length + 1

  create: ->
    if @dir
      mkdirRecursive @path

  rel: (r) ->
    @path.slice (r or root).length + 1

  onPromise = undefined

  on: (name, listener) ->
    addListener.call @, name, listener
    onPromise = new Promise (resolve) =>
      if name is 'add*' or name is 'change*' or name is 'remove*'
        await @watch()
      else if name is 'add' or name is 'change' or name is 'remove'
        await @getParent().on name + '*', listener.parent = (entry, content) =>
          if entry.path is @path
            @emit name, content
      resolve()

  once: (name, listener) ->
    if name is 'add' or name is 'change' or name is 'remove'
      staredName = name + '*'
      
      listener.parent = (entry, content) =>
        if entry.path is @path
          @emit name, content
      
      listener.wrap = (content) =>
        listener.call @, content
        removeListener.call @, name, listener.wrap
        removeListener.call @getParent(), staredName, listener.parent
      
      addListener.call @getParent(), staredName, listener.parent
      addListener.call @, name, listener.wrap
      
      @getParent().watch()
    else
      addOnceListener.call @, name, listener
      onPromise

  removeListener: (name, listener) ->
    if name is 'add*' or name is 'change*' or name is 'remove*'
      removeListener.call @, name, listener
      @unwatch()
    else
      if name is 'add' or name is 'change' or name is 'remove'
        @getParent().removeListener name + '*', listener.parent
        @getParent().unwatch()
        listener = listener.wrap if listener.wrap
      removeListener.call @, name, listener

  new: (name) ->
    @constructor.new @getSlashed() + name, name

  get: (name) ->
    @constructor.get @getSlashed() + name, name

  toString: -> @path

  write: (content) -> new Promise (resolve, reject) =>
    writeFile @path, content, (err) =>
      unless err
        @content = content
        return resolve()
      return reject err if err.code isnt 'ENOENT'
      try
        await mkdirRecursive @path.slice 0, @path.lastIndexOf sep
      catch err
        return reject err
      writeFile @path, content, (err) =>
        return reject err if err
        @content = content
        resolve()

  remove: (name) ->
    if name
      if (i = @listNames[name])?
        @list[i].remove()
    else
      parent = @getParent()
      name = @filename
      unless @hasWatchListeners()
        @stopWatcher() if @watcher
        delete map[@path]
      if list = parent.list
        listNames = parent.listNames
        list.splice listNames[name], 1
        pos = listNames[name]
        delete listNames[name]
        len = list.length
        while pos < len
          listNames[list[pos].filename] = pos
          pos++
      parent.emit 'remove*', @

  watch: ->
    return @watcher if @watcher
    @watcher = @createWatcher()

  createWatcher: ->
    await @ls()
    if @watcher is false
      delete @watcher
      return
    path = @getSlashed()
    @watcher = watch path, @watchHandler.bind @

  watchHandler: (type, name) ->
    if type is 'rename'
      @handleRename name
    else if type is 'change'
      @handleChange name

  handleRename: (name) ->
    if name.length is 0 or name is @filename and not await @mtime
      @remove() if @watcher
    else
      entry = await @get name
      if entry
        return if @listNames[name]?
        i = @list.length
        @list[i] = entry
        @listNames[name] = i
        @emit 'add*', entry
      else if @listNames[name]?
        @remove name

  handleChange: (name) ->
    i = @listNames[name]
    unless i?
      return
    entry = @list[i]
    if entry.changing
      return
    entry.changing = true
    delete entry.content
    @emit 'change*', entry, await entry.read()
    entry.changing = false

  unwatch: ->
    dir = if @dir then @ else @getParent()
    dir.stopWatcher() if dir.watcher and not dir.hasWatchListeners()
    @

  stopWatcher: ->
    if @watcher instanceof Promise
      @watcher = false
    else
      @watcher.close()
      delete @watcher

  unlink: -> new Promise (resolve, reject) =>
    unlink @path, (err) =>
      return reject err if err and err.code isnt 'ENOENT'
      resolve @

  getfurtherExisting: (requested, dir) -> new Promise (resolve) =>
    len = @path.length + 1
    if ~(i = requested.path.indexOf sep, len)
      path = requested.path.slice 0, i
      name = path.slice len
      if dir = await @constructor.get path, name
        return resolve await dir.getfurtherExisting requested, dir
      return resolve [@, @constructor.new path, name]
    if entry = await @constructor.get requested.path, requested.filename
      if dir isnt true and dir isnt false or entry.dir is dir
        return resolve [entry]
    resolve [@, requested]

  getClosestExisting: (dir) -> new Promise (resolve) =>
    if await @mtime
      if dir isnt true and dir isnt false or @dir is dir
        return resolve [@]
    current = @
    while parent = current.getParent()
      if await parent.getMtime()
        return resolve [parent, current]
      current = parent

  @getStat: (path) -> new Promise (resolve, reject) ->
    stat path, (err, stat) ->
      if err then reject err
      else resolve stat
  
  getStat: -> Entry.getStat @path


  @getSize: (path) -> new Promise (resolve, reject) ->
    Entry.getStat(path).catch(reject).then (stat) ->
      resolve stat.size
  
  getSize: -> Entry.getSize @path
   
  @getMtime: (path) -> new Promise (resolve, reject) ->
    Entry.getStat(path).then((stat) ->
      resolve stat.mtime
    ).catch (err) ->
      return reject err if err.code isnt 'ENOENT'
      resolve false
  
  getMtime: -> new Promise (resolve, reject) =>
    Entry.getStat(@path).then((stat) =>
      @dir = stat.isDirectory()
      resolve @mtimeValue = stat.mtime
    ).catch (err) =>
      return reject err if err.code isnt 'ENOENT'
      delete map[@path]
      resolve @mtimeValue = false

  getSlashed: ->
    if @path.length is 0 then @path else @path + sep

  exist: -> new Promise (resolve) =>
    resolve await(@getMtime()) and true or false

  hasWatchListeners: ->
    @listenerCount('add*') isnt 0 or
    @listenerCount('change*') isnt 0 or
    @listenerCount('remove*') isnt 0
  
  getParent: ->
    if @parent
      return @parent
    @parent = @constructor.new @path.slice 0, @path.lastIndexOf sep
    @parent.dir = true
    @parent

  read: ->
    return @content if @content?
    new Promise (resolve) =>
      readFile @path, 'utf8', (err, content) =>
        throw new Error err if err
        resolve @content = content
  
  ls: ->
    return Promise.resolve @list if @list and @watcher
    unless @listenerCount 'ls'
      path = @getSlashed()
      readdir path, (error, list) =>
        return @emit 'ls', {error} if error
        listNames = (@listNames or= {})
        delete listNames[k] for k of listNames
        len = list.length
        if @list
          @list.length = len
        else
          @list = list
        return @emit 'ls', @list unless len
        list.forEach (name, i) =>
          entry = await @constructor.get path + name, name
          @list[i] = entry
          listNames[name] = i
          if --len is 0
            @emit 'ls', @list
    new Promise (resolve, reject) =>
      @once 'ls', (list) ->
        if list.error
          reject list.error
        else
          resolve list

Entry.root = new Entry(
  root
  root.slice 1 + root.lastIndexOf sep
)
Entry.root.dir = true
Entry.root.mtime = Infinity
