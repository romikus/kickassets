module.exports = class Dependency
  constructor: (resolver, path, value) ->
    if typeof value is 'object'
      {rule, handler} = value
    rule or= rule or resolver.rule
    handler or= resolver.handler
    entry = resolver.kickassets.Entry.new path
    @resolver = rule.findOrCreateResolver entry, handler

  isSatisfied: ->
    unless @satisfied?
      await @getSatisfiedPromise()
    @satisfied

  getSatisfiedPromise: ->
    unless @satisfiedPromise
      @satisfiedPromise = @setSatisfied()
    @satisfiedPromise

  setSatisfied: ->
    results = await Promise.all [
      @resolver.entry.exist()
      @resolver.getDependencies().areSatisfied()
    ]
    @satisfied = results[0] and results[1]
    delete @satisfiedPromise
    return

  getMtime: ->
    @resolver.getMtime()

  watch: ->
    @resolver.watch()

  unwatch: ->
    @resolver.unwatch()
