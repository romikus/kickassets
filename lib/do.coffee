{isArray} = Array
Rule = require './rule'
{toA} = require './helpers'

toH = (value) ->
  if typeof value is 'string'
    value = default: value
  value

module.exports = (rules) ->
  if arguments.length isnt 1
    [from, to, handlers, options] = arguments
  else if isArray rules
    [from, to, handlers, options] = rules
  else
    for name, rule of rules
      if isArray rule
        [from, to, handlers, options] = rule
      else
        {from, to} = rule
        delete rule.from
        delete rule.to
        if rule.handler
          handlers = [rule.handler]
          delete rule.handler
        else if rule.handlers
          {handlers} = rule
          delete rule.handlers
      @rules.push new Rule @, toA(from), toH(to), toA(handlers), rule, name
    return

  handlers = [handlers] if handlers and not isArray handlers
  @rules.push new Rule @, toA(from), toH(to), handlers, options or {}
