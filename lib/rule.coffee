{isArray} = Array

Rule = require './rule'
{sep, resolve} = require 'path'
cwd = process.cwd()
cwdLen = cwd.length + 1
{toA} = require './helpers'
copyHandler = require './handler/copy'

module.exports = class Rule
  constructor: (@kickassets, from, to, handlers, options, name) ->
    @from = from
    @to = to
    @options = options
    @name = name
    @offsets = {}
    @handlers = []
    @resolvers = []
    @setRoot options.root
    @setSearch options.search

    for path, i in @from
      path += sep + '**' unless ~path.indexOf '**'
      @from[i] = path
      @offsets[path] = @root.length + 1 + path.indexOf '**'

    if handlers
      for handler in handlers
        handler = @kickassets[handler] if typeof handler is 'string'
        @handlers.push handler

    if options.copy
      @handlers.push copyHandler
    return

  setRoot: (root = @kickassets.root) ->
    if root.length is 0
      @root = cwd
    else if root[0] isnt sep
      @root = cwd + sep + root
    return

  setSearch: (search) ->
    @search = toA search
    for s, i in @search
      @search[i] = @root + sep + s

  findOrCreateResolver: (entry, handler) ->
    handlerPos = @handlers.indexOf handler
    resolvers = @resolvers[handlerPos]
    unless resolvers
      @resolvers[handlerPos] = resolvers = {}

    resolver = resolvers[entry.path]
    unless resolver
      resolver = new @kickassets.Resolver entry, @, handler, @search
      resolvers[entry.path] = resolver
    resolver
