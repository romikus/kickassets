module.exports = class Compiler
  RulePatternCompiler: require './rule_pattern_compiler'

  constructor: (@kickassets) ->

  setWatch: (@watch) ->

  compile: ->
    promises = []
    for rule in @kickassets.rules
      for pattern in rule.from
        rulePatternCompiler = @createRulePatternCompiler rule, pattern
        promises.push rulePatternCompiler.compile()
    Promise.all promises

  createRulePatternCompiler: (rule, pattern) ->
    rulePatternCompiler = new @RulePatternCompiler @kickassets, rule, pattern
    rulePatternCompiler.setWatch(@watch)
    rulePatternCompiler
