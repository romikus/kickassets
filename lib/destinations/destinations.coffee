module.exports = class Destinations
  constructor: (@kickassets, @resolver, pattern, suffix) ->
    @map = {}
    for key, handlerDestination of @resolver.handler.destinations
      @map[key] = new @kickassets.Destination @resolver, key, pattern, suffix

  exist: ->
    return @existValue if @existValue?
    promises = []
    for key, destination of @map
      promises.push destination.exist()
    for entry in await Promise.all promises
      unless entry
        return @existValue = false
    @existValue = true

  getMtime: ->
    return @mtime if @mtime?
    promises = []
    for key, dest of @map
      promises.push dest.getMtime()
    for mtime in await Promise.all promises
      return @mtime = false unless mtime
      @mtime = mtime if not @mtime or mtime < @mtime
    @mtime

  write: (results) ->
    promises = []
    for key, destination of @map
      result = results[key]
      if result
        promises.push @map[key].write result
    Promise.all promises
