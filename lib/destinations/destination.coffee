Entry = require '../entry'
{sep} = require 'path'
createDirPathBuilder = require './create_dir_path_builder'

module.exports = class Destination
  HASH_LENGTH: 64 + 1
  DirPathBuilders: {}

  constructor: (@resolver, @type, pattern, suffix) ->
    @setSuffixAndFilename suffix
    @setDir pattern

  setSuffixAndFilename: (suffix) ->
    @suffix = @resolver.handler.destinations[@type].suffix or ''
    filename = @resolver.entry.filename
    filename = filename.slice 0, -suffix.length if suffix
    dotPos = filename.indexOf '.'
    if dotPos is -1
      @name = filename
    else
      @name = filename.slice 0, dotPos
      @suffix = filename.slice(dotPos) + @suffix
    return

  setDir: (pattern) ->
    buildDirPath = @findOrCreateDirPathBuilder()
    relativeDir = @getRelativeDir pattern
    dirPath = buildDirPath pattern, relativeDir
    @dir = Entry.new dirPath
    return

  findOrCreateDirPathBuilder: ->
    path = @resolver.rule.to[@type] or @resolver.rule.to.default
    path = @resolver.rule.root + sep + path
    buildDirPath = @DirPathBuilders[path]
    unless buildDirPath
      @DirPathBuilders[path] = buildDirPath = createDirPathBuilder path
    buildDirPath

  getRelativeDir: (pattern) ->
    @resolver.entry.getParent().path.slice @resolver.rule.offsets[pattern]

  exist: ->
    @entry = @dir.new await @getFilename()
    @entry.exist()

  getMtime: ->
    @entry.getMtime()

  write: (result) ->
    if @entry
      Promise.all [@unlink(), @writeNewFile result]
    else
      @writeNewFile result

  unlink: ->
    promise = @entry.unlink()
    @entry = null
    promise

  getFilename: ->
    @name + @suffix

  getPath: ->
    @dir.getSlashed() + await @getFilename()

  writeNewFile: (result) ->
    @entry = @dir.new await @getFilename()
    await @entry.write result
    return
