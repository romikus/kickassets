require './helpers'
{root, sep} = require './entry'
{isArray} = Array
cwd = process.cwd()
cwdLen = cwd.length + 1

cut = (pattern) ->
  if pattern.indexOf(cwd) is 0
    pattern.slice cwd.length + 1
  else
    pattern

match = (str, s, ptr, p) ->
  loop
    switch ptr[p]
      when undefined
        return str[s] is undefined
      when '*'
        i = 0
        loop
          if match str, s + i, ptr, p + 1
            return true
          unless str[s + i]
            return false
          i++
      when '?'
        unless str[s]
          return false
      else
        if str[s] isnt ptr[p]
          return false
    s++
    p++

module.exports = class FileSearcher extends require 'events'
  constructor: (@pattern) ->
    super()

  compare: (patterns, index, entry, event, i) ->
    pattern = patterns[index++]
    if not entry.dir and not pattern
      @emit event, entry, i
      return
    promises = []
    if pattern is '**'
      promises.push @doubleStar patterns, index, entry, event, i
      pattern = patterns[index++]
    if pattern
      if ~pattern.indexOf('*') or ~pattern.indexOf('?')
        await @compareDir patterns, index, entry, event, i, pattern, promises
      else
        await @compareEntry patterns, index, entry, event, i, pattern
    await Promise.all promises

  compareDir: (patterns, index, entry, event, i, pattern, promises) ->
    unless entry.dir
      return

    if @watch
      entry.on 'add*', (entry) =>
        if match entry.filename, 0, pattern, 0
          @compare patterns, index, entry, 'add', i
  
    for entry in await entry.ls()
      if match entry.filename, 0, pattern, 0
        promises.push @compare patterns, index, entry, event, i

  compareEntry: (patterns, index, entry, event, i, pattern) ->
    entry = entry.new pattern
    if @watch
      entry.on 'add', (entry) ->
        @compare patterns, index, entry, 'add', i
    if await entry.exist()
      await @compare patterns, index, entry, event, i

  doubleStar: (patterns, index, entry, event, i) ->
    if @watch
      entry.on 'add*', (entry) =>
        @compare patterns, index, entry, 'add', i
        if entry.dir
          @doubleStar patterns, index, entry, 'add', i
    promises = []
    for entry in await entry.ls()
      if entry.dir
        promises.push @compare(patterns, index, entry, event, i),
          @doubleStar patterns, index, entry, event, i
    await Promise.all promises

  start: ->
    @watch = @listenerCount('add') isnt 0
    if isArray @pattern
      await Promise.all (
        for p, i in @pattern
          @compare cut(p).split(sep), 0, root, 'find', i
      )
    else
      await @compare cut(@pattern).split(sep), 0, root, 'find', 0
    @emit 'ready'
