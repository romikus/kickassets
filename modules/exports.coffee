module.exports = (kickassets) ->

  {onchange, process, processNewer} = kickassets.Resolver::

  kickassets.Resolver::onchange = (content) ->
    exported = @export
    delete @export
    await @isExporting()
    onchange.call @, content
    if exported and not @export
      @unlinkDest()
    return

  kickassets.Resolver::process = ->
    if await @isExporting()
      await process.call @
    return

  kickassets.Resolver::processNewer = (reporter) ->
    [exist] = await Promise.all [@destinations.exist(), @isExporting()]
    unless @export
      if exist
        try
          await @unlinkDest()
        catch err
          return reporter.add 'unlinkError', @
        reporter.add 'unlink', @
      else
        reporter.add 'ignore', @
      return
    await processNewer.call @, reporter

  kickassets.Resolver::isExporting = ->
    if @export?
    then @export
    else if typeof @handler.config.export is 'boolean'
    then @export = @handler.config.export
    else if @handler.config.export
    then @export = await @handler.config.export @
    else @export = true
