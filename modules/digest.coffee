crypto = require 'crypto'

setDigest = ->
  handlerDigest = @resolver.handler.digest
  if handlerDigest
    if handlerDigest is true
      @digest = true
      return
    else if typeof handlerDigest is 'string'
      @digest = @type is 'default'
      return
    else
      for _, type of handlerDigest
        if type is @type
          @digest = true
          return
  @digest = false
  return

module.exports = (kickassets) ->
  class DigestDestination extends kickassets.Destination
    constructor: (resolver, type, pattern, suffix) ->
      super resolver, type, pattern, suffix
      @setDigest()

    setDigest: setDigest

    setEntry: ->
      unless @digest
        return super()

      for entry in await @dir.ls()
        dotPos = entry.filename.indexOf '.'
        if dotPos is -1
          entryName = entry.filename.slice 0, -@HASH_LENGTH
          entrySuffix = ''
        else
          entryName = entry.filename.slice 0, dotPos - @HASH_LENGTH
          entrySuffix = entry.filename.slice dotPos

        if entryName is @name and entrySuffix is @suffix
          @entry = entry
          return true
      
      false

    exist: ->
      unless @digest
        return super()

      @setEntry()

    getFilenameFromResult = (result) ->
      hash = crypto.createHmac('sha256', result).digest 'hex'
      @name + '-' + hash + @suffix

    getFilename: ->
      unless @digest
        return super()

      results = await @resolver.getResults()
      result = results[@type]
      getFilenameFromResult.call @, result

    writeNewFile: (result) ->
      unless @digest
        return super result

      @entry = @dir.new getFilenameFromResult.call @, result
      await @entry.write result
      return

  kickassets.Destination = DigestDestination
