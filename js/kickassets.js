(function () {
  'use strict';

  var Kickassets, assetPath, digest, exports$1;

  process.on('unhandledRejection', function(reason, p) {
    return console.error('Unhandled Rejection at:', p, 'reason:', reason);
  });

  exports$1 = require('./modules/exports');

  digest = require('./modules/digest');

  assetPath = require('./modules/asset_path');

  module.exports = Kickassets = (function() {
    class Kickassets {
      constructor() {
        this.delete = false;
        this.rules = [];
      }

      static start(fn) {
        return new Kickassets().start(fn);
      }

      handle() {
        var handler, handlers, j, len, name, results;
        results = [];
        for (j = 0, len = arguments.length; j < len; j++) {
          handlers = arguments[j];
          results.push((function() {
            var results1;
            results1 = [];
            for (name in handlers) {
              handler = handlers[name];
              results1.push(this[name] = new this.Handler(name, handler));
            }
            return results1;
          }).call(this));
        }
        return results;
      }

      async start(fn) {
        this.setDefaultModules();
        this.parseArguments();
        this.configure(fn);
        this.setReporter();
        await this.run();
        return this.report();
      }

      setDefaultModules() {
        var fn;
        fn = this.Resolver.prototype.getDependencies;
        return this.use(exports$1, digest, assetPath);
      }

      parseArguments() {
        var arg, i, j, k, len, ref, ref1;
        ref = process.argv;
        for (j = 0, len = ref.length; j < len; j++) {
          arg = ref[j];
          if (arg[0] === '-') {
            for (i = k = 1, ref1 = arg.length; (1 <= ref1 ? k < ref1 : k > ref1); i = 1 <= ref1 ? ++k : --k) {
              switch (arg[i]) {
                case 'd':
                  this.delete = true;
                  break;
                case 'w':
                  this.action = 'watch';
                  break;
                case 'c':
                  this.action = 'compile';
                  break;
                default:
                  throw new Error(`Expected \`r\`, \`w\` and/or \`d\` flags, got ${arg}`);
              }
            }
          }
        }
        if (!this.delete && !this.action) {
          return this.action = 'watch';
        }
      }

      configure(fn) {
        fn.call(this);
      }

      async run() {
        if (this.delete) {
          await this.actions.delete.call(this);
        }
        return (await this.actions[this.action].call(this));
      }

      use() {
        var j, len, module;
        for (j = 0, len = arguments.length; j < len; j++) {
          module = arguments[j];
          module(this);
        }
      }

      setReporter() {
        return this.reporter = new this.Report;
      }

      report() {
        return this.reporter.print();
      }

    }
    Kickassets.prototype.Destination = require('./lib/destinations/destination');

    Kickassets.prototype.Destinations = require('./lib/destinations/destinations');

    Kickassets.prototype.Entry = require('./lib/entry');

    Kickassets.prototype.Compiler = require('./lib/compiler/compiler');

    Kickassets.prototype.FileSearcher = require('./lib/file_searcher');

    Kickassets.prototype.Rule = require('./lib/rule');

    Kickassets.prototype.Handler = require('./lib/handler/handler');

    Kickassets.prototype.Resolver = require('./lib/resolver/resolver');

    Kickassets.prototype.Pipe = require('./lib/resolver/pipe');

    Kickassets.prototype.Chain = require('./lib/resolver/chain');

    Kickassets.prototype.Dependencies = require('./lib/dependencies/dependencies');

    Kickassets.prototype.Dependency = require('./lib/dependencies/dependency');

    Kickassets.prototype.Report = require('./lib/report');

    Kickassets.prototype.root = '';

    Kickassets.prototype.do = require('./lib/do');

    Kickassets.prototype.copy = require('./lib/handler/copy');

    Kickassets.prototype.actions = {
      watch: function() {
        var compiler;
        compiler = new this.Compiler(this);
        compiler.setWatch(true);
        return compiler.compile();
      },
      compile: function() {
        return new this.Compiler(this).compile();
      }
    };

    return Kickassets;

  }).call(undefined);

}());
