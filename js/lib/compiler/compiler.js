(function () {
  'use strict';

  var Compiler;

  module.exports = Compiler = (function() {
    class Compiler {
      constructor(kickassets) {
        this.kickassets = kickassets;
      }

      setWatch(watch) {
        this.watch = watch;
      }

      compile() {
        var i, j, len, len1, pattern, promises, ref, ref1, rule, rulePatternCompiler;
        promises = [];
        ref = this.kickassets.rules;
        for (i = 0, len = ref.length; i < len; i++) {
          rule = ref[i];
          ref1 = rule.from;
          for (j = 0, len1 = ref1.length; j < len1; j++) {
            pattern = ref1[j];
            rulePatternCompiler = this.createRulePatternCompiler(rule, pattern);
            promises.push(rulePatternCompiler.compile());
          }
        }
        return Promise.all(promises);
      }

      createRulePatternCompiler(rule, pattern) {
        var rulePatternCompiler;
        rulePatternCompiler = new this.RulePatternCompiler(this.kickassets, rule, pattern);
        rulePatternCompiler.setWatch(this.watch);
        return rulePatternCompiler;
      }

    }
    Compiler.prototype.RulePatternCompiler = require('./rule_pattern_compiler');

    return Compiler;

  }).call(undefined);

}());
