(function () {
  'use strict';

  var RulePatternCompiler, sep;

  ({sep} = require('path'));

  module.exports = RulePatternCompiler = class RulePatternCompiler {
    constructor(kickassets, rule, pattern) {
      this.kickassets = kickassets;
      this.rule = rule;
      this.pattern = pattern;
    }

    setWatch(watch) {
      this.watch = watch;
    }

    async compile() {
      this.promises = [];
      this.setFileSearcher();
      this.handleSearchFind();
      if (this.watch) {
        this.handleSearchAdd();
      }
      await this.searcher.start();
      await Promise.all(this.promises);
    }

    setFileSearcher() {
      var fileSearcherPattern;
      fileSearcherPattern = this.rule.root + sep + this.pattern + sep + '*';
      this.searcher = new this.kickassets.FileSearcher(fileSearcherPattern);
    }

    handleSearchFind() {
      this.searcher.on('find', this.onfind.bind(this));
    }

    handleSearchAdd() {
      this.searcher.on('add', this.onadd.bind(this));
    }

    onfind(entry) {
      var p, resolver;
      resolver = this.createResolver(entry);
      if (!resolver) {
        return;
      }
      if (this.watch) {
        resolver.watch();
      }
      p = resolver.processNewer(this.kickassets.reporter);
      this.promises.push(p);
    }

    onadd(entry) {
      var resolver;
      resolver = this.setResolver(entry);
      if (!resolver) {
        return;
      }
      resolver.watch();
      resolver.process();
    }

    createResolver(entry) {
      var defaultHandler, handler, i, j, len, len1, len2, path, ref, resolver, suffix, suffixes;
      path = entry.path;
      len = path.length;
      ref = this.rule.handlers;
      for (i = 0, len1 = ref.length; i < len1; i++) {
        handler = ref[i];
        if (suffixes = handler.suffixes) {
          for (j = 0, len2 = suffixes.length; j < len2; j++) {
            suffix = suffixes[j];
            if (path.lastIndexOf(suffix) === len - suffix.length) {
              resolver = this.rule.findOrCreateResolver(entry, handler);
              resolver.setDestinations(this.pattern, suffix);
              return resolver;
            }
          }
        } else {
          defaultHandler = handler;
        }
      }
      if (defaultHandler) {
        resolver = this.rule.findOrCreateResolver(entry, defaultHandler);
        resolver.setDestinations(this.pattern);
        return resolver;
      }
      return false;
    }

  };

}());
