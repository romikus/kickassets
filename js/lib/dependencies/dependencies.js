(function () {
  'use strict';

  var Dependencies;

  module.exports = Dependencies = class Dependencies {
    constructor(kickassets, resolver1) {
      this.kickassets = kickassets;
      this.resolver = resolver1;
    }

    async get() {
      if (this.map == null) {
        await this.getMapPromise();
      }
      return this.map;
    }

    async areSatisfied() {
      if (this.satisfied == null) {
        await this.getSatisfiedPromise();
      }
      return this.satisfied;
    }

    getSatisfiedPromise() {
      if (!this.satisfiedPromise) {
        this.satisfiedPromise = this.setSatisfied();
      }
      return this.satisfiedPromise;
    }

    async setSatisfied() {
      var dependency, i, len, path, promises, ref, ref1, satisfied;
      promises = [];
      ref = (await this.get());
      for (path in ref) {
        dependency = ref[path];
        promises.push(dependency.isSatisfied());
      }
      this.satisfied = true;
      ref1 = (await Promise.all(promises));
      for (i = 0, len = ref1.length; i < len; i++) {
        satisfied = ref1[i];
        if (!satisfied) {
          this.satisfied = false;
          break;
        }
      }
      delete this.satisfiedPromise;
    }

    getMtimes() {
      var dependency, path, ref, results;
      ref = this.map;
      results = [];
      for (path in ref) {
        dependency = ref[path];
        results.push(dependency.getMtime());
      }
      return results;
    }

    getMapPromise() {
      if (!this.promise) {
        this.promise = this.make();
        if (this.promise) {
          this.promise.then(this.makeSuccess.bind(this));
          this.promise.catch(this.makeError.bind(this));
        }
      }
      return this.promise;
    }

    async makeSuccess(map) {
      this.map = map;
      if (this.map) {
        await this.build();
      } else {
        this.map = {};
      }
      return this.deletePromise();
    }

    makeError(err) {
      this.map = false;
      console.error(err);
      return this.deletePromise();
    }

    deletePromise() {
      return delete this.promise;
    }

    make() {
      if (!this.resolver.handler.config.dependencies) {
        this.map = {};
        return;
      }
      return this.resolver.handler.config.dependencies(this.resolver);
    }

    build() {
      var dependency, path, ref, value;
      ref = this.map;
      for (path in ref) {
        value = ref[path];
        dependency = new this.kickassets.Dependency(this.resolver, path, value);
        this.map[path] = dependency;
      }
    }

    async watch() {
      var dependency, path, ref;
      this.changeListeners = {};
      this.removeListeners = {};
      await this.get();
      ref = this.map;
      for (path in ref) {
        dependency = ref[path];
        dependency.watch();
        this.changeListeners[path] = this.changeListener.bind(this);
        this.removeListeners[path] = this.removeListener.bind(this);
        dependency.resolver.on('change', this.changeListeners[path]);
        dependency.resolver.once('remove', this.removeListeners[path]);
      }
    }

    unwatch() {
      var dependency, path, ref;
      ref = this.dependencies;
      for (path in ref) {
        dependency = ref[path];
        dependency.unwatch();
        dependency.resolver.removeListener('change', this.changeListeners[path]);
        dependency.resolver.removeListener('remove', this.removeListeners[path]);
      }
    }

    async changeListener(resolver, changeCount) {
      if (this.resolver.lastChanged !== resolver || this.resolver.lastChangedCount !== changeCount) {
        this.resolver.lastChanged = resolver;
        this.resolver.lastChangedCount = changeCount;
        this.resolver.emit('change', resolver, changeCount);
        delete this.resolver.results;
        if ((await this.areSatisfied())) {
          this.resolver.process();
        }
      }
    }

    removeListener(resolver) {
      var dependency, path, ref;
      ref = this.map;
      for (path in ref) {
        dependency = ref[path];
        if (dependency.resolver === resolver) {
          dependency.satisfied = false;
          resolver.removeListener('change', this.changeListeners[path]);
          delete this.changeListeners[path];
          delete this.removeListeners[path];
          this.resolver.emit('remove', resolver);
          delete this.resolver.results;
          this.resolver.unlinkDest();
          break;
        }
      }
    }

  };

}());
