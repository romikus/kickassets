(function () {
  'use strict';

  var Dependency;

  module.exports = Dependency = class Dependency {
    constructor(resolver, path, value) {
      var entry, handler, rule;
      if (typeof value === 'object') {
        ({rule, handler} = value);
      }
      rule || (rule = rule || resolver.rule);
      handler || (handler = resolver.handler);
      entry = resolver.kickassets.Entry.new(path);
      this.resolver = rule.findOrCreateResolver(entry, handler);
    }

    async isSatisfied() {
      if (this.satisfied == null) {
        await this.getSatisfiedPromise();
      }
      return this.satisfied;
    }

    getSatisfiedPromise() {
      if (!this.satisfiedPromise) {
        this.satisfiedPromise = this.setSatisfied();
      }
      return this.satisfiedPromise;
    }

    async setSatisfied() {
      var results;
      results = (await Promise.all([this.resolver.entry.exist(), this.resolver.getDependencies().areSatisfied()]));
      this.satisfied = results[0] && results[1];
      delete this.satisfiedPromise;
    }

    getMtime() {
      return this.resolver.getMtime();
    }

    watch() {
      return this.resolver.watch();
    }

    unwatch() {
      return this.resolver.unwatch();
    }

  };

}());
