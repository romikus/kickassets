(function () {
  'use strict';

  var Entry, Resolver, defaultError, defaultSuccess, process, watch;

  Entry = require('../entry');

  watch = require('./watch');

  process = require('./process');

  ({
    success: defaultSuccess,
    error: defaultError
  } = require('./default_messages'));

  module.exports = Resolver = (function() {
    var unlink;

    class Resolver extends require('events') {
      constructor(entry1, rule, handler) {
        super();
        this.entry = entry1;
        this.rule = rule;
        this.handler = handler;
        this.kickassets = this.rule.kickassets;
        this.watching = 0;
      }

      setDestinations(pattern, suffix) {
        this.pattern = pattern;
        this.suffix = suffix;
        this.destinations = new this.kickassets.Destinations(this.kickassets, this, this.pattern, this.suffix);
      }

      getRelativePath() {
        if (!this.relativePath) {
          this.setRelativePath();
        }
        return this.relativePath;
      }

      setRelativePath() {
        this.relativePath = this.entry.path.slice(this.rule.offsets[this.pattern]);
        if (this.suffix) {
          this.relativePath = this.relativePath.slice(0, -this.suffix.length);
        }
      }

      async processNewer(reporter) {
        var err, event, exist, existPromise, mtime, promises, satisfied, satisfiedPromise;
        existPromise = this.destinations.exist();
        satisfiedPromise = this.getDependencies().areSatisfied();
        [exist, satisfied] = (await Promise.all([existPromise, satisfiedPromise]));
        if (!satisfied) {
          reporter.add('unsatisfied', this);
          return;
        }
        if (!exist) {
          event = 'create';
        } else {
          mtime = (await this.getMtime());
          if (mtime > (await this.destinations.getMtime())) {
            event = 'update';
          } else {
            reporter.add('untouched', this);
            return;
          }
        }
        try {
          await this.process(promises = []);
          await Promise.all(promises);
        } catch (error) {
          err = error;
          return reporter.add(`${event}Error`, this, err);
        }
        reporter.add(event, this);
      }

      getDependencies() {
        if (!this.dependencies) {
          this.setDependencies();
        }
        return this.dependencies;
      }

      setDependencies() {
        this.dependencies = new this.kickassets.Dependencies(this.kickassets, this);
      }

      async getMtime() {
        var times;
        if (this.mtime != null) {
          return this.mtime;
        }
        if (!this.dependencies.areSatisfied()) {
          return false;
        }
        times = this.dependencies.getMtimes();
        times.push(this.entry.getMtime());
        return this.mtime = Math.max.apply(Math, (await Promise.all(times)));
      }

      async read() {
        if (this.content == null) {
          this.setContent((await this.entry.read()));
        }
        return this.content;
      }

      setContent(content) {
        this.content = content;
      }

      getResolver() {
        return this;
      }

      async process(promises) {
        await process(this, promises);
      }

      write(results) {
        if (this.destinations) {
          return new Promise((resolve, reject) => {
            return this.destinations.write(results).then(() => {
              this.success();
              return resolve();
            }).catch((err) => {
              this.error(err);
              return reject();
            });
          });
        } else {
          return this.success();
        }
      }

      unlinkDeep(promises, arr, resolver, destination) {
        var i, item, len, results1;
        results1 = [];
        for (i = 0, len = arr.length; i < len; i++) {
          item = arr[i];
          item.resolver = resolver;
          item.setDest(destination);
          results1.push(promises.push(item.unlinkDest()));
        }
        return results1;
      }

      async unlinkDest() {
        var destination, key, promises, ref, ref1, ref2, ref3, value;
        promises = [];
        if (this.destinations) {
          ref = this.destinations.map;
          for (key in ref) {
            destination = ref[key];
            promises.push(unlink.call(this, destination));
          }
          ref1 = this.handler.map;
          for (key in ref1) {
            value = ref1[key];
            if (value.pipes) {
              ref2 = this.destinations.map;
              for (key in ref2) {
                destination = ref2[key];
                unlinkDeep(promises, value.pipes, this.getResolver(), destination);
              }
            }
            if (value.chains) {
              ref3 = this.destinations.map;
              for (key in ref3) {
                destination = ref3[key];
                unlinkDeep(promises, value.chains, this.getResolver(), destination);
              }
            }
          }
        }
        await Promise.all(promises);
        return this.emit('unlink');
      }

      async getResults() {
        if (this.results == null) {
          await this.setResults();
        }
        return this.results;
      }

      async setResults() {
        this.results = (await this.getPipedResults());
      }

      async getPipedResults(type) {
        var result, results;
        result = (await this.handler.config.make(this));
        if (typeof result !== 'object' || Buffer.isBuffer(result)) {
          results = {};
          results[type || 'default'] = result;
        } else {
          results = result;
        }
        await this.kickassets.Pipe.process(this, results);
        return results;
      }

      success(destination) {
        if (this.handler.config.success) {
          return this.handler.config.success(this, destination);
        } else {
          return defaultSuccess(this, destination);
        }
      }

      error(err) {
        if (this.handler.config.error) {
          return this.handler.config.error(this, err);
        } else {
          return defaultError(this, err);
        }
      }

    }
    Object.assign(Resolver.prototype, watch);

    unlink = function(destination) {
      return destination.unlink().then((entry) => {
        this.emit('unlink');
        return console.log(`remove ${entry.path}`);
      });
    };

    return Resolver;

  }).call(undefined);

}());
