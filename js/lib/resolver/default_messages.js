(function () {
  'use strict';

  module.exports = {
    success: function(resolver, destination) {
      var from, to;
      if (destination) {
        from = resolver.entry.rel(resolver.rule.root);
        to = destination.entry.rel(resolver.rule.root);
        return console.info(`${resolver.handler.name}: ` + `${from} -> ${to}\n`);
      } else {
        return console.info(`${resolver.handler.name}: ` + `${resolver.entry.rel(resolver.rule.root)}\n`);
      }
    },
    error: function(resolver, err) {
      return console.error(err);
    }
  };

}());
