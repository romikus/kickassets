(function () {
  'use strict';

  var Chain, process;

  process = require('./process');

  module.exports = Chain = class Chain {
    constructor(handler) {
      this.handler = handler;
      this.handler.setDestinations();
    }

    static extendHandler(handler, args) {
      var arg, base, base1, base2, chain, chains, h, i, j, k, len, len1, map;
      h = handler.extend();
      for (j = 0, len = args.length; j < len; j++) {
        arg = args[j];
        if (arg.constructor.name === 'Handler') {
          ((base = ((base1 = h.map).all || (base1.all = {}))).chains || (base.chains = [])).push(new Chain(arg));
        } else {
          for (i in arg) {
            chains = arg[i];
            map = ((base2 = h.map)[i] || (base2[i] = {}));
            if (!Array.isArray(chains)) {
              chains = [chains];
            }
            for (k = 0, len1 = chains.length; k < len1; k++) {
              chain = chains[k];
              (map.chains || (map.chains = [])).push(new Chain(chain));
            }
          }
        }
      }
      return h;
    }

    static process(resolver, results, promises) {
      var chain, j, len, map, ref, result, type;
      if (!resolver.destinations) {
        return;
      }
      map = resolver.handler.map;
      for (type in results) {
        result = results[type];
        if (map[type] && map[type].chains) {
          ref = map[type].chains;
          for (j = 0, len = ref.length; j < len; j++) {
            chain = ref[j];
            chain.process(type, result, resolver, promises);
          }
        }
      }
    }

    async process(type1, content, resolver1, promises) {
      this.type = type1;
      this.content = content;
      this.resolver = resolver1;
      this.kickassets = this.resolver.kickassets;
      await process(this, promises);
    }

    read() {
      return this.content;
    }

    async write(results) {
      var entry, result;
      result = results[this.type];
      entry = (await this.getEntry());
      if (result != null) {
        await entry.write(result);
      } else {
        await entry.unlink();
      }
    }

    async getEntry() {
      var filename, handlerDestination, resolverDestination;
      handlerDestination = this.getDestination();
      resolverDestination = this.getResolverDestination();
      filename = (await resolverDestination.getFilename());
      filename += handlerDestination.suffix;
      return resolverDestination.dir.new(filename);
    }

    getDestination() {
      return this.handler.destinations[this.type] || this.handler.destinations.default;
    }

    getResolverDestination() {
      return this.resolver.destinations.map[this.type] || this.resolver.destinations.map.default;
    }

    getResults() {
      return this.kickassets.Resolver.prototype.getPipedResults.call(this, this.type);
    }

  };

}());
