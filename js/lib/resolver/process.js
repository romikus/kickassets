(function () {
  'use strict';

  var processChains, processSelf, pushPromisesToCallee, write;

  module.exports = async function(resolver, calleePromises) {
    var promises, results;
    promises = [];
    results = (await processSelf(resolver));
    write(resolver, results, promises);
    processChains(resolver, promises, results);
    pushPromisesToCallee(promises, calleePromises);
  };

  processSelf = async function(resolver) {
    var err, results;
    try {
      results = (await resolver.getResults());
    } catch (error) {
      err = error;
      console.error(err);
      resolver.error(err);
      throw err;
    }
    return results;
  };

  write = function(resolver, results, promises) {
    return promises.push(resolver.write(results));
  };

  processChains = function(resolver, promises, results) {
    return resolver.kickassets.Chain.process(resolver, results, promises);
  };

  pushPromisesToCallee = function(promises, calleePromises) {
    if (calleePromises) {
      return Array.prototype.push.apply(calleePromises, promises);
    }
  };

}());
