(function () {
  'use strict';

  module.exports = {
    watch: function() {
      if (this.watching) {
        return;
      }
      this.watching++;
      this.watchEntry();
      return this.watchDependencies();
    },
    unwatch: function() {
      if (!this.watching) {
        return;
      }
      this.watching--;
      this.unwatchEntry();
      this.unwatchDependencies();
    },
    watchEntry: function() {
      this.changeCount = 0;
      this.entry.on('change', this.entryChangeListener = this.onchange.bind(this));
      this.entry.once('remove', this.entryRemoveListener = this.onremove.bind(this));
    },
    unwatchEntry: function() {
      this.entry.removeListener('change', this.entryChangeListener);
      this.entry.removeListener('remove', this.entryRemoveListener);
      delete this.entryChangeListener;
      return delete this.entryRemoveListener;
    },
    onchange: async function(content) {
      this.unwatchDependencies();
      this.setContent(content);
      delete this.results;
      delete this.dependencies.map;
      this.watchDependencies();
      this.changeCount++;
      this.lastChanged = this;
      this.lastChangedCount = this.changeCount;
      this.emit('change', this, this.changeCount);
      if ((await this.dependencies.areSatisfied())) {
        this.process();
      }
    },
    onremove: function() {
      this.unwatchEntry();
      this.unwatchDependencies();
      this.emit('remove', this);
      this.unlinkDest();
    },
    watchDependencies: async function() {
      this.dependencies = (await this.getDependencies());
      await this.dependencies.watch();
    },
    unwatchDependencies: function() {
      this.dependencies.unwatch();
    }
  };

}());
