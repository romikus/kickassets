(function () {
  'use strict';

  var Pipe;

  module.exports = Pipe = class Pipe {
    constructor(handler1) {
      this.handler = handler1;
    }

    static extendHandler(handler, args) {
      var arg, base, base1, base2, h, i, j, k, len, len1, map, pipe, pipes;
      h = handler.extend();
      for (j = 0, len = args.length; j < len; j++) {
        arg = args[j];
        if (arg.constructor.name === 'Handler') {
          ((base = ((base1 = h.map).all || (base1.all = {}))).pipes || (base.pipes = [])).push(new Pipe(arg));
        } else {
          for (i in arg) {
            pipes = arg[i];
            map = ((base2 = h.map)[i] || (base2[i] = {}));
            if (!Array.isArray(pipes)) {
              pipes = [pipes];
            }
            for (k = 0, len1 = pipes.length; k < len1; k++) {
              pipe = pipes[k];
              (map.pipes || (map.pipes = [])).push(new Pipe(pipe));
            }
          }
        }
      }
      return h;
    }

    static async process(resolver, results) {
      var processed;
      processed = (await this.processResults(resolver, results));
      this.replaceResults(results, processed);
    }

    static processResults(resolver, results) {
      var map, result, type;
      map = resolver.handler.map;
      return Promise.all((function() {
        var results1;
        results1 = [];
        for (type in results) {
          result = results[type];
          results1.push(this.processAllAndType(resolver, map, type, result));
        }
        return results1;
      }).call(this));
    }

    static replaceResults(results, processed) {
      var i, type;
      i = 0;
      for (type in results) {
        results[type] = processed[i++];
      }
    }

    static async processAllAndType(resolver, map, type, result) {
      result = (await this.processAll(resolver, map.all, type, result));
      result = (await this.processType(resolver, map[type], type, result));
      return result;
    }

    static async processAll(resolver, all, type, result) {
      if (all && all.pipes) {
        result = (await this.processResult(resolver, all.pipes, type, result));
      }
      return result;
    }

    static async processType(resolver, hash, type, result) {
      if (hash && hash.pipes) {
        result = (await this.processResult(resolver, hash.pipes, type, result));
      }
      return result;
    }

    static async processResult(resolver, pipes, type, result) {
      var j, len, pipe, results;
      for (j = 0, len = pipes.length; j < len; j++) {
        pipe = pipes[j];
        pipe.content = result;
        pipe.kickassets = resolver.kickassets;
        results = (await pipe.kickassets.Resolver.prototype.getPipedResults.call(pipe, type));
        result = results[type];
        if (result == null) {
          result = results.default;
        }
      }
      return result;
    }

    read() {
      return this.content;
    }

  };

}());
