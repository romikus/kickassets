(function () {
  'use strict';

  var Report, colors, paint, reportMessages;

  paint = function(msg, c) {
    return `${c}${msg}\x1b[0m`;
  };

  reportMessages = {
    create: 'Created: ',
    update: 'Updated: ',
    unlink: 'Unlinked: ',
    untouched: 'Untouched: ',
    createError: 'Create failed: ',
    updateError: 'Update failed: ',
    unlinkError: 'Unlink failed: ',
    unsatisfied: 'Unsatisfied: ',
    ignore: 'Ignored: ',
    absent: 'Requires one of: '
  };

  colors = {
    white: "\x1b[97m",
    green: "\x1b[32m",
    yellow: "\x1b[33m",
    cyan: "\x1b[36m",
    orange: "\x1b[35m",
    blue: "\x1b[34m",
    red: "\x1b[91m"
  };

  colors.fromTo = colors.yellow;

  colors.path = colors.white;

  colors.ruleName = colors.yellow;

  colors.handlerName = colors.white;

  colors.handlersPipe = colors.orange;

  colors.resolversPipe = colors.yellow;

  colors.create = colors.green;

  colors.update = colors.yellow;

  colors.ignore = colors.cyan;

  colors.unlink = colors.orange;

  colors.untouched = colors.blue;

  colors.absent = colors.red;

  colors.unsatisfied = colors.red;

  colors.createError = colors.red;

  colors.updateError = colors.red;

  colors.unlinkError = colors.red;

  module.exports = Report = (function() {
    class Report {
      add(status, resolver, error) {
        var h, handler, j, k, len, len1, r, ref, ref1, rule, target;
        ref = this.rules;
        for (j = 0, len = ref.length; j < len; j++) {
          r = ref[j];
          if (resolver.rule.isPrototypeOf(r)) {
            target = r;
            break;
          }
        }
        if (target) {
          rule = target;
        } else {
          this.rules.push(rule = Object.create(resolver.rule));
          rule.handlers = [];
        }
        target = null;
        ref1 = rule.handlers;
        for (k = 0, len1 = ref1.length; k < len1; k++) {
          h = ref1[k];
          if (resolver.handler.isPrototypeOf(h)) {
            target = h;
            break;
          }
        }
        if (target) {
          handler = target;
        } else {
          rule.handlers.push(handler = Object.create(resolver.handler));
          handler.resolvers = [];
        }
        handler.resolvers.push(resolver = Object.create(resolver));
        resolver.status = status;
        resolver.error = error;
      }

      print() {
        var j, len, ref, rule;
        ref = this.rules;
        for (j = 0, len = ref.length; j < len; j++) {
          rule = ref[j];
          this.printRule(rule);
        }
        process.stdout.write(this.result.join(''));
      }

      printRule(rule) {
        var dest, dests, handler, i, j, key, last, len, ref;
        this.result.push(paint('╔═ ', colors.handlersPipe));
        if (rule.name) {
          this.result.push(paint(rule.name, colors.ruleName) + ' ');
        }
        dests = (function() {
          var ref, results;
          ref = rule.to;
          results = [];
          for (key in ref) {
            dest = ref[key];
            results.push(dest);
          }
          return results;
        })();
        this.result.push(paint(rule.from.join(','), colors.path), paint(' => ', colors.fromTo), paint(dests.join(', '), colors.path), '\n');
        last = rule.handlers.length - 1;
        ref = rule.handlers;
        for (i = j = 0, len = ref.length; j < len; i = ++j) {
          handler = ref[i];
          this.printHandler(handler, i === last);
        }
      }

      printHandler(handler, lastHandler) {
        var i, j, last, len, prefix, ref, resolver, sign;
        sign = lastHandler ? '╚═ ' : '╠═ ';
        this.result.push(paint(sign, colors.handlersPipe));
        this.result.push(this.getHandlerInfo(handler), '\n');
        if (lastHandler) {
          prefix = '   ';
        } else {
          prefix = paint('║  ', colors.handlersPipe);
        }
        last = handler.resolvers.length - 1;
        ref = handler.resolvers;
        for (i = j = 0, len = ref.length; j < len; i = ++j) {
          resolver = ref[i];
          this.printResolver(resolver, i === last, prefix);
        }
      }

      getHandlerInfo(handler) {
        var chains, map, pipes, s;
        ({map} = handler);
        if (map) {
          pipes = this.getPipesInfo(map);
          chains = this.getChainsInfo(map);
        }
        s = paint(`${handler.name}`, colors.handlerName);
        if (pipes) {
          s += pipes;
        }
        if (pipes || chains) {
          s = `(${s})`;
        }
        if (chains) {
          s += chains;
        }
        return s;
      }

      getPipesInfo(map) {
        var j, key, len, pipe, pipes, ref, value;
        pipes = '';
        for (key in map) {
          value = map[key];
          if (value.pipes) {
            ref = value.pipes;
            for (j = 0, len = ref.length; j < len; j++) {
              pipe = ref[j];
              pipes += ' | ';
              if (key !== 'all') {
                pipes += key + ': ';
              }
              pipes += this.getHandlerInfo(pipe.handler);
            }
          }
        }
        return pipes;
      }

      getChainsInfo(map) {
        var chain, chains, j, key, len, ref, value;
        chains = '';
        for (key in map) {
          value = map[key];
          if (value.chains) {
            ref = value.chains;
            for (j = 0, len = ref.length; j < len; j++) {
              chain = ref[j];
              chains += ' -> ';
              if (key !== 'all') {
                chains += key + ': ';
              }
              chains += this.getHandlerInfo(chain.handler);
            }
          }
        }
        return chains;
      }

      printResolver(resolver, lastResolver, prefix) {
        var dest, key, ref, status;
        ({status} = resolver);
        this.result.push(prefix, lastResolver ? '└' : '├', "─ ", colors.resolversPipe);
        this.result.push(paint(reportMessages[status], colors[status]), paint(resolver.entry.rel(resolver.rule.root), colors.path));
        if (resolver.error) {
          this.result.push("\n    " + resolver.error + resolver.error.stack);
        }
        if (status === 'create' || status === 'update' || status === 'unlink') {
          ref = resolver.destinations.map;
          for (key in ref) {
            dest = ref[key];
            this.result.push(paint(' -> ', colors.resolversPipe), paint(dest.entry.rel(resolver.rule.root), colors.path));
          }
        }
        return this.result.push('\n');
      }

    }
    Report.prototype.rules = [];

    Report.prototype.result = [];

    return Report;

  }).call(undefined);

}());
