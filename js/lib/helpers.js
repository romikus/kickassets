(function () {
  'use strict';

  var isArray;

  ({isArray} = Array);

  module.exports = {
    toA: function(arg) {
      if (!arg) {
        return [];
      }
      if (isArray(arg)) {
        return arg;
      } else {
        return [arg];
      }
    }
  };

}());
