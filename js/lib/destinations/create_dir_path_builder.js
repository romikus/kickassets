(function () {
  'use strict';

  var endingDefis, endingUnderscore, join, sep, wildcards;

  ({join, sep} = require('path'));

  endingDefis = new RegExp(sep + '-$');

  endingUnderscore = new RegExp(sep + '_$');

  wildcards = /\*+\/?/g;

  module.exports = function(to) {
    var shift, shiftFrom, unshiftFrom;
    shift = 0;
    while (endingDefis.test(to)) {
      to = to.slice(0, -2);
      shift++;
    }
    unshiftFrom = endingUnderscore.test(to);
    if (unshiftFrom) {
      to = to.slice(0, -2);
    }
    shiftFrom = 0;
    while (endingDefis.test(to)) {
      to = to.slice(0, -2);
      shiftFrom++;
    }
    return function(from, path) {
      var i, res;
      res = to;
      i = 0;
      while (i < shift) {
        path = path.slice(1 + path.indexOf('/'));
        i++;
      }
      i = 0;
      while (i < shiftFrom) {
        from = from.slice(1 + from.indexOf('/'));
        i++;
      }
      if (unshiftFrom) {
        res += '/' + from.replace(wildcards, '');
      }
      return join(res, path);
    };
  };

}());
