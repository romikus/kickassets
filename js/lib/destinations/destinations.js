(function () {
  'use strict';

  var Destinations;

  module.exports = Destinations = class Destinations {
    constructor(kickassets, resolver, pattern, suffix) {
      var handlerDestination, key, ref;
      this.kickassets = kickassets;
      this.resolver = resolver;
      this.map = {};
      ref = this.resolver.handler.destinations;
      for (key in ref) {
        handlerDestination = ref[key];
        this.map[key] = new this.kickassets.Destination(this.resolver, key, pattern, suffix);
      }
    }

    async exist() {
      var destination, entry, i, key, len, promises, ref, ref1;
      if (this.existValue != null) {
        return this.existValue;
      }
      promises = [];
      ref = this.map;
      for (key in ref) {
        destination = ref[key];
        promises.push(destination.exist());
      }
      ref1 = (await Promise.all(promises));
      for (i = 0, len = ref1.length; i < len; i++) {
        entry = ref1[i];
        if (!entry) {
          return this.existValue = false;
        }
      }
      return this.existValue = true;
    }

    async getMtime() {
      var dest, i, key, len, mtime, promises, ref, ref1;
      if (this.mtime != null) {
        return this.mtime;
      }
      promises = [];
      ref = this.map;
      for (key in ref) {
        dest = ref[key];
        promises.push(dest.getMtime());
      }
      ref1 = (await Promise.all(promises));
      for (i = 0, len = ref1.length; i < len; i++) {
        mtime = ref1[i];
        if (!mtime) {
          return this.mtime = false;
        }
        if (!this.mtime || mtime < this.mtime) {
          this.mtime = mtime;
        }
      }
      return this.mtime;
    }

    write(results) {
      var destination, key, promises, ref, result;
      promises = [];
      ref = this.map;
      for (key in ref) {
        destination = ref[key];
        result = results[key];
        if (result) {
          promises.push(this.map[key].write(result));
        }
      }
      return Promise.all(promises);
    }

  };

}());
