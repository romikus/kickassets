(function () {
  'use strict';

  var Destination, Entry, createDirPathBuilder, sep;

  Entry = require('../entry');

  ({sep} = require('path'));

  createDirPathBuilder = require('./create_dir_path_builder');

  module.exports = Destination = (function() {
    class Destination {
      constructor(resolver, type, pattern, suffix) {
        this.resolver = resolver;
        this.type = type;
        this.setSuffixAndFilename(suffix);
        this.setDir(pattern);
      }

      setSuffixAndFilename(suffix) {
        var dotPos, filename;
        this.suffix = this.resolver.handler.destinations[this.type].suffix || '';
        filename = this.resolver.entry.filename;
        if (suffix) {
          filename = filename.slice(0, -suffix.length);
        }
        dotPos = filename.indexOf('.');
        if (dotPos === -1) {
          this.name = filename;
        } else {
          this.name = filename.slice(0, dotPos);
          this.suffix = filename.slice(dotPos) + this.suffix;
        }
      }

      setDir(pattern) {
        var buildDirPath, dirPath, relativeDir;
        buildDirPath = this.findOrCreateDirPathBuilder();
        relativeDir = this.getRelativeDir(pattern);
        dirPath = buildDirPath(pattern, relativeDir);
        this.dir = Entry.new(dirPath);
      }

      findOrCreateDirPathBuilder() {
        var buildDirPath, path;
        path = this.resolver.rule.to[this.type] || this.resolver.rule.to.default;
        path = this.resolver.rule.root + sep + path;
        buildDirPath = this.DirPathBuilders[path];
        if (!buildDirPath) {
          this.DirPathBuilders[path] = buildDirPath = createDirPathBuilder(path);
        }
        return buildDirPath;
      }

      getRelativeDir(pattern) {
        return this.resolver.entry.getParent().path.slice(this.resolver.rule.offsets[pattern]);
      }

      async exist() {
        this.entry = this.dir.new((await this.getFilename()));
        return this.entry.exist();
      }

      getMtime() {
        return this.entry.getMtime();
      }

      write(result) {
        if (this.entry) {
          return Promise.all([this.unlink(), this.writeNewFile(result)]);
        } else {
          return this.writeNewFile(result);
        }
      }

      unlink() {
        var promise;
        promise = this.entry.unlink();
        this.entry = null;
        return promise;
      }

      getFilename() {
        return this.name + this.suffix;
      }

      async getPath() {
        return this.dir.getSlashed() + (await this.getFilename());
      }

      async writeNewFile(result) {
        this.entry = this.dir.new((await this.getFilename()));
        await this.entry.write(result);
      }

    }
    Destination.prototype.HASH_LENGTH = 64 + 1;

    Destination.prototype.DirPathBuilders = {};

    return Destination;

  }).call(undefined);

}());
