(function () {
  'use strict';

  var Handler;

  Handler = require('./handler');

  module.exports = new Handler('copy', {
    make: function(resolver) {
      return resolver.read();
    }
  });

}());
