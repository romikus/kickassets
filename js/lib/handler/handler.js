(function () {
  'use strict';

  var Chain, Handler, Pipe, extend;

  Pipe = require('../resolver/pipe');

  Chain = require('../resolver/chain');

  extend = require('./extend');

  module.exports = Handler = class Handler extends require('events') {
    constructor(name, config) {
      var key, value;
      super();
      this.name = name;
      for (key in config) {
        value = config[key];
        this[key] = value;
      }
      this.config = config;
      this.setSuffixes();
      this.setDestinations();
      this.setMap();
    }

    extend(argument) {
      var chain, config, h, key, pipe, ref, value;
      if (argument) {
        config = extend(this.config, argument);
      } else {
        config = {};
        Object.assign(config, this.config);
      }
      h = new Handler(this.name, config);
      ref = this.map;
      for (key in ref) {
        value = ref[key];
        h.map[key] = {};
        if (value.pipes) {
          h.map[key].pipes = (function() {
            var j, len, ref1, results;
            ref1 = value.pipes;
            results = [];
            for (j = 0, len = ref1.length; j < len; j++) {
              pipe = ref1[j];
              results.push(pipe);
            }
            return results;
          })();
        }
        if (value.chains) {
          h.map[key].chains = (function() {
            var j, len, ref1, results;
            ref1 = value.chains;
            results = [];
            for (j = 0, len = ref1.length; j < len; j++) {
              chain = ref1[j];
              results.push(chain);
            }
            return results;
          })();
        }
      }
      return h;
    }

    pipe() {
      return Pipe.extendHandler(this, arguments);
    }

    chain() {
      return Chain.extendHandler(this, arguments);
    }

    setSuffixes() {
      var i, j, len, ref, results, suffix;
      if (this.suffix) {
        this.suffixes = [this.suffix];
      }
      if (this.suffixes) {
        ref = this.suffixes;
        results = [];
        for (i = j = 0, len = ref.length; j < len; i = ++j) {
          suffix = ref[i];
          if (suffix[0] !== '.') {
            results.push(this.suffixes[i] = '.' + suffix);
          } else {
            results.push(void 0);
          }
        }
        return results;
      }
    }

    setDestinations() {
      var key, ref, results, value;
      if (this.destination) {
        this.destinations = {
          default: this.destination
        };
      }
      if (this.destinations) {
        ref = this.destinations;
        results = [];
        for (key in ref) {
          value = ref[key];
          if (typeof value === 'string') {
            if (value[0] !== '.') {
              value = '.' + value;
            }
            value = {
              suffix: value
            };
          }
          results.push(this.destinations[key] = value);
        }
        return results;
      } else {
        return this.destinations = {
          default: {}
        };
      }
    }

    setMap() {
      var chain, key, m, map, pipe, ref, value;
      map = {};
      if (this.map) {
        ref = this.map;
        for (key in ref) {
          value = ref[key];
          m = map[key] = {};
          if (value.pipes) {
            m.pipes = (function() {
              var j, len, ref1, results;
              ref1 = value.pipes;
              results = [];
              for (j = 0, len = ref1.length; j < len; j++) {
                pipe = ref1[j];
                results.push(pipe);
              }
              return results;
            })();
          }
          if (value.chains) {
            m.chains = (function() {
              var j, len, ref1, results;
              ref1 = value.chains;
              results = [];
              for (j = 0, len = ref1.length; j < len; j++) {
                chain = ref1[j];
                results.push(chain);
              }
              return results;
            })();
          }
        }
      }
      return this.map = map;
    }

  };

}());
