(function () {
  'use strict';

  var extend;

  module.exports = extend = function(a, b) {
    var key, out, value;
    out = {};
    if (typeof a === 'object') {
      Object.assign(out, a);
    }
    for (key in b) {
      value = b[key];
      if (typeof value === 'object') {
        out[key] = extend(out[key], value);
      } else {
        out[key] = value;
      }
    }
    return out;
  };

}());
