(function () {
  'use strict';

  var Rule, isArray, toA, toH;

  ({isArray} = Array);

  Rule = require('./rule');

  ({toA} = require('./helpers'));

  toH = function(value) {
    if (typeof value === 'string') {
      value = {
        default: value
      };
    }
    return value;
  };

  module.exports = function(rules) {
    var from, handlers, name, options, rule, to;
    if (arguments.length !== 1) {
      [from, to, handlers, options] = arguments;
    } else if (isArray(rules)) {
      [from, to, handlers, options] = rules;
    } else {
      for (name in rules) {
        rule = rules[name];
        if (isArray(rule)) {
          [from, to, handlers, options] = rule;
        } else {
          ({from, to} = rule);
          delete rule.from;
          delete rule.to;
          if (rule.handler) {
            handlers = [rule.handler];
            delete rule.handler;
          } else if (rule.handlers) {
            ({handlers} = rule);
            delete rule.handlers;
          }
        }
        this.rules.push(new Rule(this, toA(from), toH(to), toA(handlers), rule, name));
      }
      return;
    }
    if (handlers && !isArray(handlers)) {
      handlers = [handlers];
    }
    return this.rules.push(new Rule(this, toA(from), toH(to), handlers, options || {}));
  };

}());
