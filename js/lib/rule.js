(function () {
  'use strict';

  var Rule, copyHandler, cwd, cwdLen, resolve, sep, toA;

  Rule = require('./rule');

  ({sep, resolve} = require('path'));

  cwd = process.cwd();

  cwdLen = cwd.length + 1;

  ({toA} = require('./helpers'));

  copyHandler = require('./handler/copy');

  module.exports = Rule = class Rule {
    constructor(kickassets, from, to, handlers, options, name) {
      var handler, i, j, k, len, len1, path, ref;
      this.kickassets = kickassets;
      this.from = from;
      this.to = to;
      this.options = options;
      this.name = name;
      this.offsets = {};
      this.handlers = [];
      this.resolvers = [];
      this.setRoot(options.root);
      this.setSearch(options.search);
      ref = this.from;
      for (i = j = 0, len = ref.length; j < len; i = ++j) {
        path = ref[i];
        if (!~path.indexOf('**')) {
          path += sep + '**';
        }
        this.from[i] = path;
        this.offsets[path] = this.root.length + 1 + path.indexOf('**');
      }
      if (handlers) {
        for (k = 0, len1 = handlers.length; k < len1; k++) {
          handler = handlers[k];
          if (typeof handler === 'string') {
            handler = this.kickassets[handler];
          }
          this.handlers.push(handler);
        }
      }
      if (options.copy) {
        this.handlers.push(copyHandler);
      }
      return;
    }

    setRoot(root = this.kickassets.root) {
      if (root.length === 0) {
        this.root = cwd;
      } else if (root[0] !== sep) {
        this.root = cwd + sep + root;
      }
    }

    setSearch(search) {
      var i, j, len, ref, results, s;
      this.search = toA(search);
      ref = this.search;
      results = [];
      for (i = j = 0, len = ref.length; j < len; i = ++j) {
        s = ref[i];
        results.push(this.search[i] = this.root + sep + s);
      }
      return results;
    }

    findOrCreateResolver(entry, handler) {
      var handlerPos, resolver, resolvers;
      handlerPos = this.handlers.indexOf(handler);
      resolvers = this.resolvers[handlerPos];
      if (!resolvers) {
        this.resolvers[handlerPos] = resolvers = {};
      }
      resolver = resolvers[entry.path];
      if (!resolver) {
        resolver = new this.kickassets.Resolver(entry, this, handler, this.search);
        resolvers[entry.path] = resolver;
      }
      return resolver;
    }

  };

}());
