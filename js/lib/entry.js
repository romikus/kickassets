(function () {
  'use strict';

  var Entry, EventEmitter, Watcher, addListener, addOnceListener, createReadStream, createWriteStream, exists, map, mkdir, mkdirPool, mkdirRecursive, readFile, readdir, removeListener, root, sep, stat, unlink, watch, writeFile;

  ({stat, watch, readdir, readFile, writeFile, unlink, exists, mkdir, createReadStream, createWriteStream} = require('fs'));

  sep = require('path').sep;

  require('./helpers');

  map = {};

  root = process.cwd();

  EventEmitter = require('events');

  EventEmitter.defaultMaxListeners = 100;

  ({
    on: addListener,
    once: addOnceListener,
    removeListener: removeListener
  } = EventEmitter.prototype);

  mkdirPool = {};

  mkdirRecursive = function(path) {
    return new Promise(function(resolve, reject) {
      return exists(path, function(exists) {
        var arr, cb;
        if (exists) {
          return resolve();
        }
        cb = async function(err) {
          if (!err || err === 'EEXIST') {
            return resolve();
          }
          if (err.code !== 'ENOENT') {
            return reject(err);
          }
          try {
            await mkdirRecursive(path.slice(0, path.lastIndexOf(sep)));
          } catch (error1) {
            err = error1;
            return reject(err);
          }
          return mkdir(path, resolve);
        };
        if (mkdirPool[path]) {
          return mkdirPool[path].push(cb);
        } else {
          mkdirPool[path] = arr = [cb];
          return mkdir(path, function(err) {
            var j, len1, results;
            delete mkdirPool[path];
            results = [];
            for (j = 0, len1 = arr.length; j < len1; j++) {
              cb = arr[j];
              results.push(cb(err));
            }
            return results;
          });
        }
      });
    });
  };

  Watcher = (function() {
    var watchRecursive;

    class Watcher extends EventEmitter {
      constructor(path1, dir1) {
        super();
        this.path = path1;
        this.dir = dir1;
        this.target = Entry.new(this.path);
      }

      close() {
        return this.closed = true;
      }

      async start() {
        var deeper, existing;
        this.watch = this.listenerCount('add') !== 0 || this.listenerCount('remove') !== 0;
        [existing, deeper] = (await this.target.getClosestExisting(this.dir));
        if (this.closed) {
          return;
        }
        if (this.watch) {
          watchRecursive.call(this, existing, deeper, 'found', 'not found');
        } else if (deeper) {
          this.emit('not found');
        } else {
          this.emit('found', existing);
        }
        if (deeper) {
          return false;
        } else {
          return existing;
        }
      }

    }
    watchRecursive = async function(existing, deeper, add, remove) {
      var onadd, onremove;
      if (deeper) { // not found
        await deeper.once('add', onadd = async() => {
          deeper.removeListener('remove', onremove);
          if (deeper === this.target) {
            existing = this.target;
            deeper = null;
          } else {
            [existing, deeper] = (await deeper.getfurtherExisting(this.target, this.dir));
          }
          return watchRecursive.call(this, existing, deeper, 'add', 'remove');
        });
        await deeper.once('remove', onremove = async() => {
          deeper.removeListener('add', onadd);
          [existing, deeper] = (await deeper.getClosestExisting(this.dir));
          return watchRecursive.call(this, existing, deeper, 'add', 'remove');
        });
        this.close = function() {
          deeper.removeListener('add', onadd);
          return deeper.removeListener('remove', onremove);
        };
        return this.emit(remove, this.target); // found
      } else {
        await this.target.once('remove', onremove = async() => {
          [existing, deeper] = (await this.target.getClosestExisting(this.dir));
          return watchRecursive.call(this, existing, deeper, 'add', 'remove');
        });
        this.close = () => {
          return this.target.removeListener('remove', onremove);
        };
        return this.emit(add, existing);
      }
    };

    return Watcher;

  }).call(undefined);

  module.exports = Entry = (function() {
    var onPromise;

    class Entry extends EventEmitter {
      constructor(path, filename) {
        super();
        this.filename = filename;
        map[this.path = path] = this;
        this.changing = false;
      }

      static new(path, name) {
        return map[path] || new Entry(path, name || path.slice(1 + path.lastIndexOf(sep)));
      }

      static async get(path, name) {
        var entry;
        entry = this.new(path, name);
        if ((await entry.exist())) {
          return entry;
        }
      }

      static watch(path, dir) {
        return new Watcher(path, dir);
      }

      static copy(from, to) {
        return new Promise(async function(resolve, reject) {
          var err;
          try {
            await mkdirRecursive(to.slice(0, to.lastIndexOf(sep)));
          } catch (error1) {
            err = error1;
            return reject(err);
          }
          return createReadStream(from).pipe(createWriteStream(to).on('close', resolve)).on('error', reject);
        });
      }

      static rel(path, r) {
        return path.slice((r || root).length + 1);
      }

      create() {
        if (this.dir) {
          return mkdirRecursive(this.path);
        }
      }

      rel(r) {
        return this.path.slice((r || root).length + 1);
      }

      on(name, listener) {
        addListener.call(this, name, listener);
        return onPromise = new Promise(async(resolve) => {
          if (name === 'add*' || name === 'change*' || name === 'remove*') {
            await this.watch();
          } else if (name === 'add' || name === 'change' || name === 'remove') {
            await this.getParent().on(name + '*', listener.parent = (entry, content) => {
              if (entry.path === this.path) {
                return this.emit(name, content);
              }
            });
          }
          return resolve();
        });
      }

      once(name, listener) {
        var staredName;
        if (name === 'add' || name === 'change' || name === 'remove') {
          staredName = name + '*';
          listener.parent = (entry, content) => {
            if (entry.path === this.path) {
              return this.emit(name, content);
            }
          };
          listener.wrap = (content) => {
            listener.call(this, content);
            removeListener.call(this, name, listener.wrap);
            return removeListener.call(this.getParent(), staredName, listener.parent);
          };
          addListener.call(this.getParent(), staredName, listener.parent);
          addListener.call(this, name, listener.wrap);
          return this.getParent().watch();
        } else {
          addOnceListener.call(this, name, listener);
          return onPromise;
        }
      }

      removeListener(name, listener) {
        if (name === 'add*' || name === 'change*' || name === 'remove*') {
          removeListener.call(this, name, listener);
          return this.unwatch();
        } else {
          if (name === 'add' || name === 'change' || name === 'remove') {
            this.getParent().removeListener(name + '*', listener.parent);
            this.getParent().unwatch();
            if (listener.wrap) {
              listener = listener.wrap;
            }
          }
          return removeListener.call(this, name, listener);
        }
      }

      new(name) {
        return this.constructor.new(this.getSlashed() + name, name);
      }

      get(name) {
        return this.constructor.get(this.getSlashed() + name, name);
      }

      toString() {
        return this.path;
      }

      write(content) {
        return new Promise((resolve, reject) => {
          return writeFile(this.path, content, async(err) => {
            if (!err) {
              this.content = content;
              return resolve();
            }
            if (err.code !== 'ENOENT') {
              return reject(err);
            }
            try {
              await mkdirRecursive(this.path.slice(0, this.path.lastIndexOf(sep)));
            } catch (error1) {
              err = error1;
              return reject(err);
            }
            return writeFile(this.path, content, (err) => {
              if (err) {
                return reject(err);
              }
              this.content = content;
              return resolve();
            });
          });
        });
      }

      remove(name) {
        var i, len, list, listNames, parent, pos;
        if (name) {
          if ((i = this.listNames[name]) != null) {
            return this.list[i].remove();
          }
        } else {
          parent = this.getParent();
          name = this.filename;
          if (!this.hasWatchListeners()) {
            if (this.watcher) {
              this.stopWatcher();
            }
            delete map[this.path];
          }
          if (list = parent.list) {
            listNames = parent.listNames;
            list.splice(listNames[name], 1);
            pos = listNames[name];
            delete listNames[name];
            len = list.length;
            while (pos < len) {
              listNames[list[pos].filename] = pos;
              pos++;
            }
          }
          return parent.emit('remove*', this);
        }
      }

      watch() {
        if (this.watcher) {
          return this.watcher;
        }
        return this.watcher = this.createWatcher();
      }

      async createWatcher() {
        var path;
        await this.ls();
        if (this.watcher === false) {
          delete this.watcher;
          return;
        }
        path = this.getSlashed();
        return this.watcher = watch(path, this.watchHandler.bind(this));
      }

      watchHandler(type, name) {
        if (type === 'rename') {
          return this.handleRename(name);
        } else if (type === 'change') {
          return this.handleChange(name);
        }
      }

      async handleRename(name) {
        var entry, i;
        if (name.length === 0 || name === this.filename && !(await this.mtime)) {
          if (this.watcher) {
            return this.remove();
          }
        } else {
          entry = (await this.get(name));
          if (entry) {
            if (this.listNames[name] != null) {
              return;
            }
            i = this.list.length;
            this.list[i] = entry;
            this.listNames[name] = i;
            return this.emit('add*', entry);
          } else if (this.listNames[name] != null) {
            return this.remove(name);
          }
        }
      }

      async handleChange(name) {
        var entry, i;
        i = this.listNames[name];
        if (i == null) {
          return;
        }
        entry = this.list[i];
        if (entry.changing) {
          return;
        }
        entry.changing = true;
        delete entry.content;
        this.emit('change*', entry, (await entry.read()));
        return entry.changing = false;
      }

      unwatch() {
        var dir;
        dir = this.dir ? this : this.getParent();
        if (dir.watcher && !dir.hasWatchListeners()) {
          dir.stopWatcher();
        }
        return this;
      }

      stopWatcher() {
        if (this.watcher instanceof Promise) {
          return this.watcher = false;
        } else {
          this.watcher.close();
          return delete this.watcher;
        }
      }

      unlink() {
        return new Promise((resolve, reject) => {
          return unlink(this.path, (err) => {
            if (err && err.code !== 'ENOENT') {
              return reject(err);
            }
            return resolve(this);
          });
        });
      }

      getfurtherExisting(requested, dir) {
        return new Promise(async(resolve) => {
          var entry, i, len, name, path;
          len = this.path.length + 1;
          if (~(i = requested.path.indexOf(sep, len))) {
            path = requested.path.slice(0, i);
            name = path.slice(len);
            if (dir = (await this.constructor.get(path, name))) {
              return resolve((await dir.getfurtherExisting(requested, dir)));
            }
            return resolve([this, this.constructor.new(path, name)]);
          }
          if (entry = (await this.constructor.get(requested.path, requested.filename))) {
            if (dir !== true && dir !== false || entry.dir === dir) {
              return resolve([entry]);
            }
          }
          return resolve([this, requested]);
        });
      }

      getClosestExisting(dir) {
        return new Promise(async(resolve) => {
          var current, parent;
          if ((await this.mtime)) {
            if (dir !== true && dir !== false || this.dir === dir) {
              return resolve([this]);
            }
          }
          current = this;
          while (parent = current.getParent()) {
            if ((await parent.getMtime())) {
              return resolve([parent, current]);
            }
            current = parent;
          }
        });
      }

      static getStat(path) {
        return new Promise(function(resolve, reject) {
          return stat(path, function(err, stat) {
            if (err) {
              return reject(err);
            } else {
              return resolve(stat);
            }
          });
        });
      }

      getStat() {
        return Entry.getStat(this.path);
      }

      static getSize(path) {
        return new Promise(function(resolve, reject) {
          return Entry.getStat(path).catch(reject).then(function(stat) {
            return resolve(stat.size);
          });
        });
      }

      getSize() {
        return Entry.getSize(this.path);
      }

      static getMtime(path) {
        return new Promise(function(resolve, reject) {
          return Entry.getStat(path).then(function(stat) {
            return resolve(stat.mtime);
          }).catch(function(err) {
            if (err.code !== 'ENOENT') {
              return reject(err);
            }
            return resolve(false);
          });
        });
      }

      getMtime() {
        return new Promise((resolve, reject) => {
          return Entry.getStat(this.path).then((stat) => {
            this.dir = stat.isDirectory();
            return resolve(this.mtimeValue = stat.mtime);
          }).catch((err) => {
            if (err.code !== 'ENOENT') {
              return reject(err);
            }
            delete map[this.path];
            return resolve(this.mtimeValue = false);
          });
        });
      }

      getSlashed() {
        if (this.path.length === 0) {
          return this.path;
        } else {
          return this.path + sep;
        }
      }

      exist() {
        return new Promise(async(resolve) => {
          return resolve((await (this.getMtime())) && true || false);
        });
      }

      hasWatchListeners() {
        return this.listenerCount('add*') !== 0 || this.listenerCount('change*') !== 0 || this.listenerCount('remove*') !== 0;
      }

      getParent() {
        if (this.parent) {
          return this.parent;
        }
        this.parent = this.constructor.new(this.path.slice(0, this.path.lastIndexOf(sep)));
        this.parent.dir = true;
        return this.parent;
      }

      read() {
        if (this.content != null) {
          return this.content;
        }
        return new Promise((resolve) => {
          return readFile(this.path, 'utf8', (err, content) => {
            if (err) {
              throw new Error(err);
            }
            return resolve(this.content = content);
          });
        });
      }

      ls() {
        var path;
        if (this.list && this.watcher) {
          return Promise.resolve(this.list);
        }
        if (!this.listenerCount('ls')) {
          path = this.getSlashed();
          readdir(path, (error, list) => {
            var k, len, listNames;
            if (error) {
              return this.emit('ls', {error});
            }
            listNames = (this.listNames || (this.listNames = {}));
            for (k in listNames) {
              delete listNames[k];
            }
            len = list.length;
            if (this.list) {
              this.list.length = len;
            } else {
              this.list = list;
            }
            if (!len) {
              return this.emit('ls', this.list);
            }
            return list.forEach(async(name, i) => {
              var entry;
              entry = (await this.constructor.get(path + name, name));
              this.list[i] = entry;
              listNames[name] = i;
              if (--len === 0) {
                return this.emit('ls', this.list);
              }
            });
          });
        }
        return new Promise((resolve, reject) => {
          return this.once('ls', function(list) {
            if (list.error) {
              return reject(list.error);
            } else {
              return resolve(list);
            }
          });
        });
      }

    }
    Entry.sep = sep;

    Entry.mkdirRecursive = mkdirRecursive;

    onPromise = void 0;

    return Entry;

  }).call(undefined);

  Entry.root = new Entry(root, root.slice(1 + root.lastIndexOf(sep)));

  Entry.root.dir = true;

  Entry.root.mtime = 2e308;

}());
