(function () {
  'use strict';

  var FileSearcher, cut, cwd, cwdLen, isArray, match, root, sep;

  require('./helpers');

  ({root, sep} = require('./entry'));

  ({isArray} = Array);

  cwd = process.cwd();

  cwdLen = cwd.length + 1;

  cut = function(pattern) {
    if (pattern.indexOf(cwd) === 0) {
      return pattern.slice(cwd.length + 1);
    } else {
      return pattern;
    }
  };

  match = function(str, s, ptr, p) {
    var i;
    while (true) {
      switch (ptr[p]) {
        case void 0:
          return str[s] === void 0;
        case '*':
          i = 0;
          while (true) {
            if (match(str, s + i, ptr, p + 1)) {
              return true;
            }
            if (!str[s + i]) {
              return false;
            }
            i++;
          }
          break;
        case '?':
          if (!str[s]) {
            return false;
          }
          break;
        default:
          if (str[s] !== ptr[p]) {
            return false;
          }
      }
      s++;
      p++;
    }
  };

  module.exports = FileSearcher = class FileSearcher extends require('events') {
    constructor(pattern1) {
      super();
      this.pattern = pattern1;
    }

    async compare(patterns, index, entry, event, i) {
      var pattern, promises;
      pattern = patterns[index++];
      if (!entry.dir && !pattern) {
        this.emit(event, entry, i);
        return;
      }
      promises = [];
      if (pattern === '**') {
        promises.push(this.doubleStar(patterns, index, entry, event, i));
        pattern = patterns[index++];
      }
      if (pattern) {
        if (~pattern.indexOf('*') || ~pattern.indexOf('?')) {
          await this.compareDir(patterns, index, entry, event, i, pattern, promises);
        } else {
          await this.compareEntry(patterns, index, entry, event, i, pattern);
        }
      }
      return (await Promise.all(promises));
    }

    async compareDir(patterns, index, entry, event, i, pattern, promises) {
      var j, len, ref, results;
      if (!entry.dir) {
        return;
      }
      if (this.watch) {
        entry.on('add*', (entry) => {
          if (match(entry.filename, 0, pattern, 0)) {
            return this.compare(patterns, index, entry, 'add', i);
          }
        });
      }
      ref = (await entry.ls());
      results = [];
      for (j = 0, len = ref.length; j < len; j++) {
        entry = ref[j];
        if (match(entry.filename, 0, pattern, 0)) {
          results.push(promises.push(this.compare(patterns, index, entry, event, i)));
        } else {
          results.push(void 0);
        }
      }
      return results;
    }

    async compareEntry(patterns, index, entry, event, i, pattern) {
      entry = entry.new(pattern);
      if (this.watch) {
        entry.on('add', function(entry) {
          return this.compare(patterns, index, entry, 'add', i);
        });
      }
      if ((await entry.exist())) {
        return (await this.compare(patterns, index, entry, event, i));
      }
    }

    async doubleStar(patterns, index, entry, event, i) {
      var j, len, promises, ref;
      if (this.watch) {
        entry.on('add*', (entry) => {
          this.compare(patterns, index, entry, 'add', i);
          if (entry.dir) {
            return this.doubleStar(patterns, index, entry, 'add', i);
          }
        });
      }
      promises = [];
      ref = (await entry.ls());
      for (j = 0, len = ref.length; j < len; j++) {
        entry = ref[j];
        if (entry.dir) {
          promises.push(this.compare(patterns, index, entry, event, i), this.doubleStar(patterns, index, entry, event, i));
        }
      }
      return (await Promise.all(promises));
    }

    async start() {
      var i, p;
      this.watch = this.listenerCount('add') !== 0;
      if (isArray(this.pattern)) {
        await Promise.all((function() {
          var j, len, ref, results;
          ref = this.pattern;
          results = [];
          for (i = j = 0, len = ref.length; j < len; i = ++j) {
            p = ref[i];
            results.push(this.compare(cut(p).split(sep), 0, root, 'find', i));
          }
          return results;
        }).call(this));
      } else {
        await this.compare(cut(this.pattern).split(sep), 0, root, 'find', 0);
      }
      return this.emit('ready');
    }

  };

}());
