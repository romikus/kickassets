(function () {
  'use strict';

  var crypto, setDigest;

  crypto = require('crypto');

  setDigest = function() {
    var _, handlerDigest, type;
    handlerDigest = this.resolver.handler.digest;
    if (handlerDigest) {
      if (handlerDigest === true) {
        this.digest = true;
        return;
      } else if (typeof handlerDigest === 'string') {
        this.digest = this.type === 'default';
        return;
      } else {
        for (_ in handlerDigest) {
          type = handlerDigest[_];
          if (type === this.type) {
            this.digest = true;
            return;
          }
        }
      }
    }
    this.digest = false;
  };

  module.exports = function(kickassets) {
    var DigestDestination;
    DigestDestination = (function() {
      var getFilenameFromResult;

      class DigestDestination extends kickassets.Destination {
        constructor(resolver, type, pattern, suffix) {
          super(resolver, type, pattern, suffix);
          this.setDigest();
        }

        async setEntry() {
          var dotPos, entry, entryName, entrySuffix, i, len, ref;
          if (!this.digest) {
            return super.setEntry();
          }
          ref = (await this.dir.ls());
          for (i = 0, len = ref.length; i < len; i++) {
            entry = ref[i];
            dotPos = entry.filename.indexOf('.');
            if (dotPos === -1) {
              entryName = entry.filename.slice(0, -this.HASH_LENGTH);
              entrySuffix = '';
            } else {
              entryName = entry.filename.slice(0, dotPos - this.HASH_LENGTH);
              entrySuffix = entry.filename.slice(dotPos);
            }
            if (entryName === this.name && entrySuffix === this.suffix) {
              this.entry = entry;
              return true;
            }
          }
          return false;
        }

        exist() {
          if (!this.digest) {
            return super.exist();
          }
          return this.setEntry();
        }

        async getFilename() {
          var result, results;
          if (!this.digest) {
            return super.getFilename();
          }
          results = (await this.resolver.getResults());
          result = results[this.type];
          return getFilenameFromResult.call(this, result);
        }

        async writeNewFile(result) {
          if (!this.digest) {
            return super.writeNewFile(result);
          }
          this.entry = this.dir.new(getFilenameFromResult.call(this, result));
          await this.entry.write(result);
        }

      }
      DigestDestination.prototype.setDigest = setDigest;

      getFilenameFromResult = function(result) {
        var hash;
        hash = crypto.createHmac('sha256', result).digest('hex');
        return this.name + '-' + hash + this.suffix;
      };

      return DigestDestination;

    }).call(this);
    return kickassets.Destination = DigestDestination;
  };

}());
