(function () {
  'use strict';

  var AssetPath, dirname, resolve;

  ({resolve, dirname} = require('path'));

  AssetPath = (function() {
    class AssetPath {
      constructor(Entry, rules) {
        this.Entry = Entry;
        this.rules = rules;
      }

      async makeDependencies(resolver) {
        var arr, content, dependencies, dependency, destination, match, name, path, quote, relative, usePathes;
        usePathes = resolver.handler.getUsePathes();
        dependencies = {};
        content = (await resolver.read());
        arr = [];
        usePathes.lastIndex = 0;
        while (match = usePathes.exec(content)) {
          name = match[1];
          if (match[2]) {
            relative = match[2];
            quote = 0;
          } else if (match[3]) {
            relative = match[3];
            quote = 1;
          } else {
            relative = match[4];
            quote = 2;
          }
          path = resolve(dirname(resolver.entry.path), relative);
          destination = this.getDependencyDestination(name, path);
          dependency = destination.resolver;
          dependencies[path] = dependency;
          arr.push(destination, match.index, match[0].length, quote);
        }
        if (arr.length) {
          resolver.assetPathData = arr;
        } else {
          delete resolver.assetPathData;
        }
        return dependencies;
      }

      getDependencyDestination(name, path) {
        var dependency, destination, handler, j, k, len1, len2, ref, ref1, rule, type;
        ref = this.rules;
        for (j = 0, len1 = ref.length; j < len1; j++) {
          rule = ref[j];
          ref1 = rule.handlers;
          for (k = 0, len2 = ref1.length; k < len2; k++) {
            handler = ref1[k];
            if (handler.pathScope) {
              if (typeof handler.pathScope === 'string') {
                type = 'default';
              } else {
                type = handler.pathScope[name];
              }
              if (type) {
                dependency = rule.findOrCreateResolver(new this.Entry(path), handler);
                destination = dependency.destinations.map[type];
                return destination;
              }
            }
          }
        }
      }

      async setResolverContent(resolver) {
        var data, i, len, path, pos;
        data = resolver.assetPathData;
        if (!data) {
          return;
        }
        this.buf.length = 0;
        i = 0;
        len = data.length;
        pos = 0;
        while (i < len) {
          path = (await data[i].getPath());
          path = path.slice(data[i].resolver.rule.root.length);
          this.buf.push(resolver.originalContent.slice(pos, data[i + 1]));
          if (data[i + 3] === 0) {
            this.buf.push('"');
            this.buf.push(path);
            this.buf.push('"');
          } else if (data[i + 3] === 1) {
            this.buf.push("'");
            this.buf.push(path);
            this.buf.push("'");
          } else if (data[i + 3] === 2) {
            this.buf.push(path);
          }
          pos = data[i + 1] + data[i + 2];
          i += 4;
        }
        this.buf.push(resolver.originalContent.slice(pos));
        resolver.content = this.buf.join('');
      }

    }
    AssetPath.prototype.buf = [];

    return AssetPath;

  }).call(undefined);

  module.exports = function(kickassets) {
    var configure, make, setContent, setResults;
    ({configure} = kickassets);
    kickassets.configure = function(fn) {
      configure.call(this, fn);
      this.assetPath = new AssetPath(this.Entry, this.rules);
    };
    kickassets.Handler.prototype.pathStringRegexp = /(?:"((?:[^"\\]|\\.)*)"|'((?:[^'\\]|\\.)*)'|([^\)]*))/;
    kickassets.Handler.prototype.getUsePathes = function() {
      if (this.preparedUsePathes == null) {
        if (this.usePath) {
          this.usePathes = [this.usePath];
        }
        if (this.usePathes) {
          this.preparedUsePathes = this.buildPathRegexes();
        } else {
          this.preparedUsePathes = false;
        }
      }
      return this.preparedUsePathes;
    };
    kickassets.Handler.prototype.buildPathRegexes = function() {
      return RegExp(`(${this.usePathes.join('|')})-path\\(${this.pathStringRegexp.source}\\)`, "g");
    };
    ({make} = kickassets.Dependencies.prototype);
    kickassets.Dependencies.prototype.make = function() {
      if (!this.resolver.handler.getUsePathes()) {
        return make.call(this);
      }
      return new Promise((resolve, reject) => {
        var out, promiseCatch, promiseThen, waiting;
        waiting = 0;
        out = {};
        promiseThen = function(dependencies) {
          if (dependencies) {
            Object.assign(out, dependencies);
          }
          if (++waiting === 2) {
            return resolve(out);
          }
        };
        promiseCatch = function(err) {
          return reject(err);
        };
        make.call(this).then(promiseThen, promiseCatch);
        return this.kickassets.assetPath.makeDependencies(this.resolver).then(promiseThen, promiseCatch);
      });
    };
    ({setContent} = kickassets.Resolver.prototype);
    kickassets.Resolver.prototype.setContent = function(content) {
      setContent.call(this, content);
      this.originalContent = this.content;
    };
    ({setResults} = kickassets.Resolver.prototype);
    return kickassets.Resolver.prototype.setResults = async function() {
      await this.kickassets.assetPath.setResolverContent(this);
      return setResults.call(this);
    };
  };

}());
