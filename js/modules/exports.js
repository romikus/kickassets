(function () {
  'use strict';

  module.exports = function(kickassets) {
    var onchange, process, processNewer;
    ({onchange, process, processNewer} = kickassets.Resolver.prototype);
    kickassets.Resolver.prototype.onchange = async function(content) {
      var exported;
      exported = this.export;
      delete this.export;
      await this.isExporting();
      onchange.call(this, content);
      if (exported && !this.export) {
        this.unlinkDest();
      }
    };
    kickassets.Resolver.prototype.process = async function() {
      if ((await this.isExporting())) {
        await process.call(this);
      }
    };
    kickassets.Resolver.prototype.processNewer = async function(reporter) {
      var exist;
      [exist] = (await Promise.all([this.destinations.exist(), this.isExporting()]));
      if (!this.export) {
        if (exist) {
          try {
            await this.unlinkDest();
          } catch (error) {
            return reporter.add('unlinkError', this);
          }
          reporter.add('unlink', this);
        } else {
          reporter.add('ignore', this);
        }
        return;
      }
      return (await processNewer.call(this, reporter));
    };
    return kickassets.Resolver.prototype.isExporting = async function() {
      if (this.export != null) {
        return this.export;
      } else if (typeof this.handler.config.export === 'boolean') {
        return this.export = this.handler.config.export;
      } else if (this.handler.config.export) {
        return this.export = (await this.handler.config.export(this));
      } else {
        return this.export = true;
      }
    };
  };

}());
