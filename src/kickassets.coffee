process.on 'unhandledRejection', (reason, p) ->
  console.error 'Unhandled Rejection at:', p, 'reason:', reason

{defineProperty} = Object

exports = require './modules/exports'
digest = require './modules/digest'
assetPath = require './modules/asset_path'

module.exports = class Kickassets

  constructor: ->
    @delete = false
    @rules = []

  @start: (fn) ->
    new Kickassets().start fn

  Destination: require './lib/destinations/destination'
  Destinations: require './lib/destinations/destinations'
  Entry: require './lib/entry'
  Compiler: require './lib/compiler/compiler'
  FileSearcher: require './lib/file_searcher'
  Rule: require './lib/rule'
  Handler: require './lib/handler/handler'
  Resolver: require './lib/resolver/resolver'
  Pipe: require './lib/resolver/pipe'
  Chain: require './lib/resolver/chain'
  Dependencies: require './lib/dependencies/dependencies'
  Dependency: require './lib/dependencies/dependency'
  Report: require './lib/report'

  root: ''
  do: require './lib/do'
  copy: require './lib/handler/copy'

  actions:
    watch: ->
      compiler = new @Compiler(@)
      compiler.setWatch(true)
      compiler.compile()
    compile: ->
      new @Compiler(@).compile()

  handle: ->
    for handlers in arguments
      for name, handler of handlers
        @[name] = new @Handler name, handler

  start: (fn) ->
    @setDefaultModules()
    @parseArguments()
    @configure fn
    @setReporter()
    await @run()
    @report()

  setDefaultModules: ->
    fn = @Resolver::getDependencies
    @use exports, digest, assetPath

  parseArguments: ->
    for arg in process.argv
      if arg[0] is '-'
        for i in [1...arg.length]
          switch arg[i]
            when 'd'
              @delete = true
            when 'w'
              @action = 'watch'
            when 'c'
              @action = 'compile'
            else
              throw new Error "Expected `r`, `w` and/or `d` flags, got #{arg}"

    if not @delete and not @action
      @action = 'watch'

  configure: (fn) ->
    fn.call @
    return

  run: ->
    if @delete
      await @actions.delete.call @
    await @actions[@action].call @

  use: ->
    for module in arguments
      module @
    return

  setReporter: ->
    @reporter = new @Report

  report: ->
    @reporter.print()
