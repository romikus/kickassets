{resolve, dirname} = require 'path'

class AssetPath
  buf: []

  constructor: (@Entry, @rules) ->

  makeDependencies: (resolver) ->
    usePathes = resolver.handler.getUsePathes()
    dependencies = {}
    content = await resolver.read()
    arr = []
    usePathes.lastIndex = 0
    while match = usePathes.exec content
      name = match[1]
      if match[2]
        relative = match[2]
        quote = 0
      else if match[3]
        relative = match[3]
        quote = 1
      else
        relative = match[4]
        quote = 2

      path = resolve dirname(resolver.entry.path), relative
      destination = @getDependencyDestination name, path
      dependency = destination.resolver
      dependencies[path] = dependency
      arr.push destination, match.index, match[0].length, quote
    
    if arr.length
      resolver.assetPathData = arr
    else
      delete resolver.assetPathData

    dependencies

  getDependencyDestination: (name, path) ->
    for rule in @rules
      for handler in rule.handlers
        if handler.pathScope
          if typeof handler.pathScope is 'string'
            type = 'default'
          else
            type = handler.pathScope[name]
          if type
            dependency = rule.findOrCreateResolver new @Entry(path), handler
            destination = dependency.destinations.map[type]
            return destination

  setResolverContent: (resolver) ->
    data = resolver.assetPathData
    unless data
      return

    @buf.length = 0
    i = 0
    len = data.length
    pos = 0
    while i < len
      path = await data[i].getPath()
      path = path.slice data[i].resolver.rule.root.length
      @buf.push resolver.originalContent.slice pos, data[i + 1]
      if data[i + 3] is 0
        @buf.push '"'
        @buf.push path
        @buf.push '"'
      else if data[i + 3] is 1
        @buf.push "'"
        @buf.push path
        @buf.push "'"
      else if data[i + 3] is 2
        @buf.push path
      pos = data[i + 1] + data[i + 2]
      i += 4

    @buf.push resolver.originalContent.slice pos
    
    resolver.content = @buf.join('')
    return

module.exports = (kickassets) ->

  {configure} = kickassets
  kickassets.configure = (fn) ->
    configure.call @, fn
    @assetPath = new AssetPath @Entry, @rules
    return

  kickassets.Handler::pathStringRegexp = ///
    (?:
      "((?:[^"\\]|\\.)*)"
      |
      '((?:[^'\\]|\\.)*)'
      |
      ([^\)]*)
    )
  ///

  kickassets.Handler::getUsePathes = ->
    unless @preparedUsePathes?
      if @usePath
        @usePathes = [@usePath]
      if @usePathes
        @preparedUsePathes = @buildPathRegexes()
      else
        @preparedUsePathes = false
    @preparedUsePathes

  kickassets.Handler::buildPathRegexes = ->
    ///
      (#{@usePathes.join '|'})-path\(
        #{@pathStringRegexp.source}
      \)
    ///g

  {make} = kickassets.Dependencies::
  kickassets.Dependencies::make = ->
    unless @resolver.handler.getUsePathes()
      return make.call @

    new Promise (resolve, reject) =>
      waiting = 0
      out = {}

      promiseThen = (dependencies) ->
        if dependencies
          Object.assign out, dependencies
        if ++waiting is 2
          resolve out

      promiseCatch = (err) ->
        reject err

      make.call(@).then promiseThen, promiseCatch
      @kickassets.assetPath.makeDependencies(@resolver).then promiseThen, promiseCatch

  {setContent} = kickassets.Resolver::
  kickassets.Resolver::setContent = (content) ->
    setContent.call @, content
    @originalContent = @content
    return

  {setResults} = kickassets.Resolver::
  kickassets.Resolver::setResults = ->
    await @kickassets.assetPath.setResolverContent @
    setResults.call @
