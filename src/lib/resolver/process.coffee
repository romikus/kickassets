module.exports = (resolver, calleePromises) ->
  promises = []
  results = await processSelf resolver
  write resolver, results, promises
  processChains resolver, promises, results
  pushPromisesToCallee promises, calleePromises
  return

processSelf = (resolver) ->
  try
    results = await resolver.getResults()
  catch err
    console.error err
    resolver.error err
    return throw err
  results

write = (resolver, results, promises) ->
  promises.push resolver.write results

processChains = (resolver, promises, results) ->
  resolver.kickassets.Chain.process resolver, results, promises

pushPromisesToCallee = (promises, calleePromises) ->
  if calleePromises
    Array.prototype.push.apply calleePromises, promises
