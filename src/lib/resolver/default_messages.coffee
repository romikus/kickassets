module.exports =
  success: (resolver, destination) ->
    if destination
      from = resolver.entry.rel resolver.rule.root
      to = destination.entry.rel resolver.rule.root
      console.info "#{resolver.handler.name}: " +
      "#{from} -> #{to}\n"
    else
      console.info "#{resolver.handler.name}: " +
      "#{resolver.entry.rel resolver.rule.root}\n"

  error: (resolver, err) ->
    console.error err
