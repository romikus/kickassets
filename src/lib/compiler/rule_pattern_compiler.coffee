{sep} = require 'path'

module.exports = class RulePatternCompiler
  constructor: (@kickassets, @rule, @pattern) ->

  setWatch: (@watch) ->

  compile: ->
    @promises = []
    @setFileSearcher()
    @handleSearchFind()
    if @watch
      @handleSearchAdd()
    await @searcher.start()
    await Promise.all @promises
    return

  setFileSearcher: ->
    fileSearcherPattern = @rule.root + sep + @pattern + sep + '*'
    @searcher = new @kickassets.FileSearcher fileSearcherPattern
    return

  handleSearchFind: ->
    @searcher.on 'find', @onfind.bind @
    return

  handleSearchAdd: ->
    @searcher.on 'add', @onadd.bind @
    return

  onfind: (entry) ->
    resolver = @createResolver entry
    unless resolver
      return
    resolver.watch() if @watch
    p = resolver.processNewer @kickassets.reporter
    @promises.push p
    return

  onadd: (entry) ->
    resolver = @setResolver entry
    unless resolver
      return
    resolver.watch()
    resolver.process()
    return

  createResolver: (entry) ->
    path = entry.path
    len = path.length
    for handler in @rule.handlers
      if suffixes = handler.suffixes
        for suffix in suffixes
          if path.lastIndexOf(suffix) is len - suffix.length
            resolver = @rule.findOrCreateResolver entry, handler
            resolver.setDestinations @pattern, suffix
            return resolver
      else
        defaultHandler = handler
    if defaultHandler
      resolver = @rule.findOrCreateResolver entry, defaultHandler
      resolver.setDestinations @pattern
      return resolver
    false
