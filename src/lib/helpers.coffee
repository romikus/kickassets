{isArray} = Array

module.exports =
  toA: (arg) ->
    return [] unless arg
    if isArray arg then arg else [arg]
