Pipe = require '../resolver/pipe'
Chain = require '../resolver/chain'
extend = require './extend'

module.exports = class Handler extends require 'events'
  constructor: (@name, config) ->
    super()
    @[key] = value for key, value of config
    @config = config
    @setSuffixes()
    @setDestinations()
    @setMap()

  extend: (argument) ->
    if argument
      config = extend @config, argument
    else
      config = {}
      Object.assign config, @config
    
    h = new Handler @name, config
    for key, value of @map
      h.map[key] = {}
      if value.pipes
        h.map[key].pipes = (pipe for pipe in value.pipes)
      if value.chains
        h.map[key].chains = (chain for chain in value.chains)
    h

  pipe: ->
    Pipe.extendHandler @, arguments

  chain: ->
    Chain.extendHandler @, arguments
  
  setSuffixes: ->
    if @suffix
      @suffixes = [@suffix]
    if @suffixes
      for suffix, i in @suffixes
        @suffixes[i] = '.' + suffix unless suffix[0] is '.'

  setDestinations: ->
    if @destination
      @destinations = default: @destination
    
    if @destinations
      for key, value of @destinations
        if typeof value is 'string'
          value = '.' + value unless value[0] is '.'
          value = suffix: value
        @destinations[key] = value
    else
      @destinations = default: {}

  setMap: ->
    map = {}
    if @map
      for key, value of @map
        m = map[key] = {}
        if value.pipes
          m.pipes = (pipe for pipe in value.pipes)
        if value.chains
          m.chains = (chain for chain in value.chains)
    @map = map
