{join, sep} = require 'path'
endingDefis = new RegExp sep + '-$'
endingUnderscore = new RegExp sep + '_$'
wildcards = /\*+\/?/g

module.exports = (to) ->
  shift = 0
  while endingDefis.test to
    to = to.slice 0, -2
    shift++
  unshiftFrom = endingUnderscore.test to
  if unshiftFrom
    to = to.slice 0, -2
  shiftFrom = 0
  while endingDefis.test to
    to = to.slice 0, -2
    shiftFrom++

  (from, path) ->
    res = to
    i = 0
    while i < shift
      path = path.slice 1 + path.indexOf '/'
      i++
    i = 0
    while i < shiftFrom
      from = from.slice 1 + from.indexOf '/'
      i++
    res += '/' + from.replace(wildcards, '') if unshiftFrom
    join res, path