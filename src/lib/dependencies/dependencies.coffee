module.exports = class Dependencies
  constructor: (@kickassets, @resolver) ->

  get: ->
    unless @map?
      await @getMapPromise()
    @map

  areSatisfied: ->
    unless @satisfied?
      await @getSatisfiedPromise()
    @satisfied

  getSatisfiedPromise: ->
    unless @satisfiedPromise
      @satisfiedPromise = @setSatisfied()
    @satisfiedPromise

  setSatisfied: ->
    promises = []
    for path, dependency of await @get()
      promises.push dependency.isSatisfied()
    @satisfied = true
    for satisfied in await Promise.all promises
      unless satisfied
        @satisfied = false
        break
    delete @satisfiedPromise
    return

  getMtimes: ->
    for path, dependency of @map
      dependency.getMtime()

  getMapPromise: ->
    unless @promise
      @promise = @make()
      if @promise
        @promise.then @makeSuccess.bind @
        @promise.catch @makeError.bind @
    @promise

  makeSuccess: (@map) ->
    if @map
      await @build()
    else
      @map = {}
    @deletePromise()

  makeError: (err) ->
    @map = false
    console.error err
    @deletePromise()

  deletePromise: ->
    delete @promise

  make: ->
    unless @resolver.handler.config.dependencies
      @map = {}
      return

    @resolver.handler.config.dependencies @resolver
  
  build: ->
    for path, value of @map
      dependency = new @kickassets.Dependency @resolver, path, value
      @map[path] = dependency
    return

  watch: ->
    @changeListeners = {}
    @removeListeners = {}
    await @get()

    for path, dependency of @map
      dependency.watch()
      @changeListeners[path] = @changeListener.bind @
      @removeListeners[path] = @removeListener.bind @
      dependency.resolver.on 'change', @changeListeners[path]
      dependency.resolver.once 'remove', @removeListeners[path]
    return

  unwatch: ->
    for path, dependency of @dependencies
      dependency.unwatch()
      dependency.resolver.removeListener 'change', @changeListeners[path]
      dependency.resolver.removeListener 'remove', @removeListeners[path]
    return

  changeListener: (resolver, changeCount) ->
    if @resolver.lastChanged isnt resolver or @resolver.lastChangedCount isnt changeCount
      @resolver.lastChanged = resolver
      @resolver.lastChangedCount = changeCount
      @resolver.emit 'change', resolver, changeCount
      delete @resolver.results
      if await @areSatisfied()
        @resolver.process()
    return

  removeListener: (resolver) ->
    for path, dependency of @map
      if dependency.resolver is resolver
        dependency.satisfied = false
        resolver.removeListener 'change', @changeListeners[path]
        delete @changeListeners[path]
        delete @removeListeners[path]
        @resolver.emit 'remove', resolver
        delete @resolver.results
        @resolver.unlinkDest()
        break
    return
